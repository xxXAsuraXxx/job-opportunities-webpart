(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["debugManifests"] = factory();
	else
		root["debugManifests"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * @copyright Microsoft Corporation. All rights reserved.
 *
 * @file manifestsFile.ts
 */
Object.defineProperty(exports, "__esModule", { value: true });
var url = __webpack_require__(1);
/**
 * Get the manifest array with each of the base URLs rewritten to point to the local
 *  page's protocol, hostname, and port. This function is useful for automated tests
 *  that run locally and use an unpredictable port.
 */
function getLocalPageManifests() {
    // Clone manifestsArray
    var manifests = JSON.parse(JSON.stringify(getManifests()));
    // tslint:disable-next-line:no-function-expression ES5 browsers don't support lambda functions
    manifests.forEach(function (manifest) {
        var baseUrls = manifest.loaderConfig.internalModuleBaseUrls;
        var baseUrl = url.parse(baseUrls[0]);
        var pageUrl = url.parse(window.location.toString());
        baseUrl.protocol = pageUrl.protocol;
        baseUrl.host = pageUrl.host;
        baseUrls[0] = url.format(baseUrl);
    });
    return manifests;
}
exports.getLocalPageManifests = getLocalPageManifests;
/**
 * Get the manifest array.
 */
function getManifests() {
    return [
  {
    "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a",
    "alias": "SPLodashSubset",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-lodash-subset",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-lodash-subset/"
      ],
      "scriptResources": {
        "sp-lodash-subset": {
          "type": "path",
          "path": "dist/sp-lodash-subset.js"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a",
    "alias": "SPLodashSubset",
    "componentType": "Library",
    "version": "1.1.0",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-lodash-subset",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/sp-client-custom-fields/node_modules/@microsoft/sp-lodash-subset/"
      ],
      "scriptResources": {
        "sp-lodash-subset": {
          "type": "path",
          "path": "dist/sp-lodash-subset.js"
        }
      }
    }
  },
  {
    "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b",
    "alias": "SPCoreLibrary",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-core-library",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-core-library/"
      ],
      "scriptResources": {
        "sp-core-library": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-core-library_en-us.js"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b",
    "alias": "SPCoreLibrary",
    "componentType": "Library",
    "version": "1.1.0",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-core-library",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/sp-client-custom-fields/node_modules/@microsoft/sp-core-library/"
      ],
      "scriptResources": {
        "sp-core-library": {
          "type": "path",
          "path": "dist/sp-core-library.js"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.1.0",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        }
      }
    }
  },
  {
    "id": "29bd516f-4ece-40b7-8028-597cbc65a223",
    "alias": "SpOfficeUIFabricCore",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "office-ui-fabric-core",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-office-ui-fabric-core/"
      ],
      "scriptResources": {
        "office-ui-fabric-core": {
          "type": "path",
          "path": "dist/office-ui-fabric-core.js"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "f97266fb-ccb7-430e-9384-4124d05295d3",
    "alias": "Decorators",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "decorators",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/decorators/"
      ],
      "scriptResources": {
        "decorators": {
          "type": "path",
          "path": "dist/decorators.js"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "02a01e42-69ab-403d-8a16-acd128661f8e",
    "alias": "OfficeUIFabricReact",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "office-ui-fabric-react-bundle",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/office-ui-fabric-react-bundle/"
      ],
      "scriptResources": {
        "office-ui-fabric-react-bundle": {
          "type": "path",
          "path": "dist/office-ui-fabric-react-bundle.js"
        },
        "react": {
          "type": "component",
          "version": "16.7.0",
          "id": "0d910c1c-13b9-4e1c-9aa4-b008c5e42d7d",
          "failoverPath": "node_modules/react/dist/react.js"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        },
        "react-dom": {
          "type": "component",
          "version": "16.7.0",
          "id": "aa0a46ec-1505-43cd-a44a-93f3a5aa460a",
          "failoverPath": "node_modules/react-dom/dist/react-dom.js"
        }
      }
    },
    "isInternal": true
  },
  {
    "manifestVersion": 2,
    "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8",
    "alias": "SPDiagnostics",
    "componentType": "Library",
    "version": "1.8.2",
    "loaderConfig": {
      "entryModuleId": "sp-diagnostics",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-diagnostics/"
      ],
      "scriptResources": {
        "sp-diagnostics": {
          "type": "path",
          "path": "dist/sp-diagnostics.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        }
      }
    },
    "isInternal": true
  },
  {
    "manifestVersion": 2,
    "id": "e40f8203-b39d-425a-a957-714852e33b79",
    "alias": "SPDynamicData",
    "componentType": "Library",
    "version": "1.8.2",
    "loaderConfig": {
      "entryModuleId": "sp-dynamic-data",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-dynamic-data/"
      ],
      "scriptResources": {
        "sp-dynamic-data": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-dynamic-data_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "1e384972-6346-49b4-93c7-b2e6763938e6",
    "alias": "sp-polyfills",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-polyfills",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-polyfills/"
      ],
      "scriptResources": {
        "sp-polyfills": {
          "type": "path",
          "path": "dist/sp-polyfills.js"
        }
      }
    }
  },
  {
    "id": "c07208f0-ea3b-4c1a-9965-ac1b825211a6",
    "alias": "SPHttp",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "preloadComponents": [
      "4d5eb168-6729-49a8-aec7-0e397f486b6e"
    ],
    "loaderConfig": {
      "entryModuleId": "sp-http",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-http/"
      ],
      "scriptResources": {
        "sp-http": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-http_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "1c4541f7-5c31-41aa-9fa8-fbc9dc14c0a8",
    "alias": "SPPageContext",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-page-context",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-page-context/"
      ],
      "scriptResources": {
        "sp-page-context": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-page-context_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/sp-dynamic-data": {
          "type": "component",
          "version": "1.8.2",
          "id": "e40f8203-b39d-425a-a957-714852e33b79"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "1c6c9123-7aac-41f3-a376-3caea41ed83f",
    "alias": "SPLoader",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-loader",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-loader/"
      ],
      "scriptResources": {
        "sp-loader": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-loader_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        },
        "@microsoft/sp-page-context": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c4541f7-5c31-41aa-9fa8-fbc9dc14c0a8"
        },
        "@microsoft/sp-http": {
          "type": "component",
          "version": "1.8.2",
          "id": "c07208f0-ea3b-4c1a-9965-ac1b825211a6"
        },
        "@ms/sp-telemetry": {
          "type": "component",
          "version": "0.4.9",
          "id": "8217e442-8ed3-41fd-957d-b112e841286a"
        },
        "@microsoft/sp-dynamic-data": {
          "type": "component",
          "version": "1.8.2",
          "id": "e40f8203-b39d-425a-a957-714852e33b79"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "467dc675-7cc5-4709-8aac-78e3b71bd2f6",
    "alias": "SPComponentBase",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-component-base",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-component-base/"
      ],
      "scriptResources": {
        "sp-component-base": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-component-base_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/sp-dynamic-data": {
          "type": "component",
          "version": "1.8.2",
          "id": "e40f8203-b39d-425a-a957-714852e33b79"
        },
        "@microsoft/decorators": {
          "type": "component",
          "version": "1.8.2",
          "id": "f97266fb-ccb7-430e-9384-4124d05295d3"
        },
        "@microsoft/sp-page-context": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c4541f7-5c31-41aa-9fa8-fbc9dc14c0a8"
        },
        "@microsoft/sp-http": {
          "type": "component",
          "version": "1.8.2",
          "id": "c07208f0-ea3b-4c1a-9965-ac1b825211a6"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "f9e737b7-f0df-4597-ba8c-3060f82380db",
    "alias": "SPPropertyPane",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-property-pane",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-property-pane/"
      ],
      "scriptResources": {
        "sp-property-pane": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-property-pane_en-us.js"
        },
        "react": {
          "type": "component",
          "version": "16.7.0",
          "id": "0d910c1c-13b9-4e1c-9aa4-b008c5e42d7d",
          "failoverPath": "node_modules/react/dist/react.js"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "react-dom": {
          "type": "component",
          "version": "16.7.0",
          "id": "aa0a46ec-1505-43cd-a44a-93f3a5aa460a",
          "failoverPath": "node_modules/react-dom/dist/react-dom.js"
        },
        "@microsoft/office-ui-fabric-react-bundle": {
          "type": "component",
          "version": "1.8.2",
          "id": "02a01e42-69ab-403d-8a16-acd128661f8e"
        },
        "@microsoft/decorators": {
          "type": "component",
          "version": "1.8.2",
          "id": "f97266fb-ccb7-430e-9384-4124d05295d3"
        },
        "@microsoft/sp-component-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "467dc675-7cc5-4709-8aac-78e3b71bd2f6"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "974a7777-0990-4136-8fa6-95d80114c2e0",
    "alias": "SPWebPartBase",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "preloadComponents": [
      "f9e737b7-f0df-4597-ba8c-3060f82380db"
    ],
    "loaderConfig": {
      "entryModuleId": "sp-webpart-base",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-webpart-base/"
      ],
      "scriptResources": {
        "sp-webpart-base": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-webpart-base_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/decorators": {
          "type": "component",
          "version": "1.8.2",
          "id": "f97266fb-ccb7-430e-9384-4124d05295d3"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        },
        "@ms/sp-telemetry": {
          "type": "component",
          "version": "0.4.9",
          "id": "8217e442-8ed3-41fd-957d-b112e841286a"
        },
        "@microsoft/sp-component-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "467dc675-7cc5-4709-8aac-78e3b71bd2f6"
        },
        "@microsoft/sp-loader": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c6c9123-7aac-41f3-a376-3caea41ed83f"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        },
        "@ms/sp-load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        },
        "@microsoft/sp-http": {
          "type": "component",
          "version": "1.8.2",
          "id": "c07208f0-ea3b-4c1a-9965-ac1b825211a6"
        },
        "@microsoft/sp-page-context": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c4541f7-5c31-41aa-9fa8-fbc9dc14c0a8"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "af59c2b3-2da7-41fd-8b72-3939817960af",
    "alias": "SPClientBase",
    "componentType": "Library",
    "version": "1.0.0",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-client-base",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-client-base/"
      ],
      "scriptResources": {
        "sp-client-base": {
          "type": "path",
          "path": "dist/sp-client-base.js"
        }
      }
    }
  },
  {
    "id": "0773bd53-a69e-4293-87e6-ba80ea4d614b",
    "alias": "SPExtensionBase",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-extension-base",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-extension-base/"
      ],
      "scriptResources": {
        "sp-extension-base": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-extension-base_en-us.js"
        },
        "@microsoft/sp-component-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "467dc675-7cc5-4709-8aac-78e3b71bd2f6"
        },
        "@ms/sp-telemetry": {
          "type": "component",
          "version": "0.4.9",
          "id": "8217e442-8ed3-41fd-957d-b112e841286a"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/decorators": {
          "type": "component",
          "version": "1.8.2",
          "id": "f97266fb-ccb7-430e-9384-4124d05295d3"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-loader": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c6c9123-7aac-41f3-a376-3caea41ed83f"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "4df9bb86-ab0a-4aab-ab5f-48bf167048fb",
    "alias": "SPApplicationBase",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "preloadComponents": [
      "c0c518b8-701b-4f6f-956d-5782772bb731",
      "02a01e42-69ab-403d-8a16-acd128661f8e"
    ],
    "loaderConfig": {
      "entryModuleId": "sp-application-base",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-application-base/"
      ],
      "scriptResources": {
        "sp-application-base": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-application-base_en-us.js"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/sp-loader": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c6c9123-7aac-41f3-a376-3caea41ed83f"
        },
        "@microsoft/sp-http": {
          "type": "component",
          "version": "1.8.2",
          "id": "c07208f0-ea3b-4c1a-9965-ac1b825211a6"
        },
        "@ms/sp-telemetry": {
          "type": "component",
          "version": "0.4.9",
          "id": "8217e442-8ed3-41fd-957d-b112e841286a"
        },
        "@microsoft/decorators": {
          "type": "component",
          "version": "1.8.2",
          "id": "f97266fb-ccb7-430e-9384-4124d05295d3"
        },
        "@microsoft/sp-extension-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "0773bd53-a69e-4293-87e6-ba80ea4d614b"
        },
        "@microsoft/sp-page-context": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c4541f7-5c31-41aa-9fa8-fbc9dc14c0a8"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        },
        "@microsoft/sp-component-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "467dc675-7cc5-4709-8aac-78e3b71bd2f6"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        },
        "@ms/sp-load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "4d5eb168-6729-49a8-aec7-0e397f486b6e",
    "alias": "SPClientPreview",
    "componentType": "Library",
    "version": "1.8.2",
    "manifestVersion": 2,
    "loaderConfig": {
      "entryModuleId": "sp-client-preview",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-client-preview/"
      ],
      "scriptResources": {
        "sp-client-preview": {
          "type": "path",
          "path": "dist/sp-client-preview.js"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "8be81a5c-af38-4bb2-af97-afa3b64dfbed",
    "alias": "WebPartWorkbench",
    "componentType": "Application",
    "version": "1.8.2",
    "manifestVersion": 2,
    "title": {
      "default": "WebpartWorkbench"
    },
    "description": {
      "default": "WebpartWorkbench"
    },
    "assemblyId": "5dae53c4-db1e-4d0b-b8b2-88c874dabf83",
    "preloadComponents": [],
    "preloadOptions": {
      "shouldPreloadWeb": true,
      "shouldPreloadUser": true,
      "shouldPreloadList": false,
      "shouldPreloadItem": true,
      "shouldPreloadQuickLaunch": true
    },
    "loaderConfig": {
      "entryModuleId": "sp-webpart-workbench",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/@microsoft/sp-webpart-workbench/"
      ],
      "scriptResources": {
        "sp-webpart-workbench": {
          "type": "localizedPath",
          "defaultPath": "dist/sp-webpart-workbench_en-us.js"
        },
        "@microsoft/office-ui-fabric-react-bundle": {
          "type": "component",
          "version": "1.8.2",
          "id": "02a01e42-69ab-403d-8a16-acd128661f8e"
        },
        "react": {
          "type": "component",
          "version": "16.7.0",
          "id": "0d910c1c-13b9-4e1c-9aa4-b008c5e42d7d",
          "failoverPath": "node_modules/react/dist/react.js"
        },
        "@microsoft/load-themed-styles": {
          "type": "component",
          "version": "0.1.2",
          "id": "229b8d08-79f3-438b-8c21-4613fc877abd"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.8.2",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-diagnostics": {
          "type": "component",
          "version": "1.8.2",
          "id": "78359e4b-07c2-43c6-8d0b-d060b4d577e8"
        },
        "@microsoft/sp-lodash-subset": {
          "type": "component",
          "version": "1.8.2",
          "id": "73e1dc6c-8441-42cc-ad47-4bd3659f8a3a"
        },
        "react-dom": {
          "type": "component",
          "version": "16.7.0",
          "id": "aa0a46ec-1505-43cd-a44a-93f3a5aa460a",
          "failoverPath": "node_modules/react-dom/dist/react-dom.js"
        },
        "@microsoft/sp-webpart-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "974a7777-0990-4136-8fa6-95d80114c2e0"
        },
        "@ms/sp-telemetry": {
          "type": "component",
          "version": "0.4.9",
          "id": "8217e442-8ed3-41fd-957d-b112e841286a"
        },
        "@microsoft/sp-loader": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c6c9123-7aac-41f3-a376-3caea41ed83f"
        },
        "@microsoft/sp-page-context": {
          "type": "component",
          "version": "1.8.2",
          "id": "1c4541f7-5c31-41aa-9fa8-fbc9dc14c0a8"
        },
        "@microsoft/sp-application-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "4df9bb86-ab0a-4aab-ab5f-48bf167048fb"
        },
        "@microsoft/sp-component-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "467dc675-7cc5-4709-8aac-78e3b71bd2f6"
        }
      }
    },
    "isInternal": true
  },
  {
    "id": "d688e552-a2fb-4904-af1c-c28aa1ee79d3",
    "alias": "TestWebPart",
    "componentType": "WebPart",
    "version": "0.0.1",
    "manifestVersion": 2,
    "preconfiguredEntries": [
      {
        "groupId": "d688e552-a2fb-4904-af1c-c28aa1ee79d3",
        "group": {
          "default": "Under Development"
        },
        "title": {
          "default": "Test"
        },
        "description": {
          "default": "Test description"
        },
        "officeFabricIconFontName": "Page",
        "properties": {
          "description": "CustomFieldsWebPart",
          "date": "",
          "date2": ""
        }
      }
    ],
    "loaderConfig": {
      "entryModuleId": "sp-client-custom-fields.bundle",
      "internalModuleBaseUrls": [
        "https://localhost:4321/node_modules/sp-client-custom-fields/"
      ],
      "scriptResources": {
        "sp-client-custom-fields.bundle": {
          "type": "path",
          "path": "dist/sp-client-custom-fields.bundle.js"
        },
        "sp-client-custom-fields/strings": {
          "defaultPath": "lib/loc/en-us.js",
          "type": "localizedPath"
        },
        "testStrings": {
          "defaultPath": "lib/webparts/test/loc/en-us.js",
          "type": "localizedPath",
          "paths": {}
        },
        "react": {
          "type": "component",
          "version": "15.4.2",
          "id": "0d910c1c-13b9-4e1c-9aa4-b008c5e42d7d",
          "failoverPath": "node_modules/react/dist/react.js"
        },
        "react-dom": {
          "type": "component",
          "version": "15.4.2",
          "id": "aa0a46ec-1505-43cd-a44a-93f3a5aa460a",
          "failoverPath": "node_modules/react-dom/dist/react-dom.js"
        },
        "@microsoft/sp-webpart-base": {
          "type": "component",
          "version": "1.1.1",
          "id": "974a7777-0990-4136-8fa6-95d80114c2e0"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.1.0",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-http": {
          "type": "component",
          "version": "1.1.1",
          "id": "c07208f0-ea3b-4c1a-9965-ac1b825211a6"
        },
        "@microsoft/sp-loader": {
          "type": "component",
          "version": "1.1.1",
          "id": "1c6c9123-7aac-41f3-a376-3caea41ed83f"
        }
      }
    }
  },
  {
    "id": "7323df8f-af6d-49a5-8159-77b2a352a3ef",
    "alias": "FphJobVacancyWebPart",
    "componentType": "WebPart",
    "version": "0.0.1",
    "manifestVersion": 2,
    "requiresCustomScript": false,
    "supportedHosts": [
      "SharePointWebPart",
      "SharePointFullPage",
      "TeamsTab"
    ],
    "supportsThemeVariants": true,
    "preconfiguredEntries": [
      {
        "groupId": "5c03119e-3074-46fd-976b-c60198311f70",
        "group": {
          "default": "Other"
        },
        "title": {
          "default": "FPH Job Vacancy"
        },
        "description": {
          "default": "FPH Job Vacancy description"
        },
        "iconImageUrl": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABG8AAAOfCAIAAAD5BHkxAAAACXBIWXMAAAsTAAALEwEAmpwYAAAgAElEQVR4nOzde5Cl6V3Y99/zXs7pnp6Z3ZW0q9XuSghZAl1AkhFCQgJzd5Utyy5TiUNSiePEiU1iJ3G4GJsyGDsWFAbs8qUMvhaEpOxy4RgoG8JFiEsACUkgdLWwsNDqtlotu9qdS3ef877Pkz9aMxrt7vT0PNvnOp9PqVSz3e/0eebMnPc93/O87/N2pZQAAADgJjWrHgAAAMBGUlMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQI1u1QMAFqKUcnh4uOpRALBw0+k0pbTqUcAtSk3Bdjo6sjq+Amy3UopdPayQM/0AAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIb7TQEREU3TuGMJwBrKOZdSVj0K4MmpKSAiIqXUdXYIAGtnGIZxHFc9CuDJOdMPAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAa3aoHACxKKeXkW7Ztu9DBAFCnlHLM/vzku3pgEcxNAZFSWvUQAAA2j5oCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqNGtegAAEPP5POe86lE8ub7vm8aHjwA8CTUFwOqVUlJKqx7FkyulrHoIAKwpNQXA6qWUIkpKcfS/dVBKlBLrOmEGwFpQUwCshbaNRz+VLl6Idj0OTfN53HlnmUxXPQ4A1th6HLIAuLXlnPu+/Lt/2//UT7aTyapHExERB/vxv//V+StfPUa0qx4LAGtKTQGwLqY7cfZc7OysehwREdG2MZ1G0zjZD4DrskgRAABADXNTAKxe0zQR4+O+WHKMOcYxlrCoXtNE20TTrssaGABsBDUFwJoaxnjhi+O+Z8cwLPyxJpN4x2/FQw9G6yIpAE5MTQGwpsYhXvWa+NIvX9LDPfpoPPiAmgLgJrhuCoA1lXPMZst7uPk8igUnALgZ5qZg+6VjLwQpS7gkBQBgG6kp2E6llJxzXBNL+QnLPDdN86S/BgDgJNQUbI9xHEspw5UL9sdxvHZWqkkpxWf+s0S5GlqllHEcZ7PZ0Vfatk0ptS4f4RZjMT8Abpaago2Xc57P50chlFJqUjpa57lNpY2miRSRItIshsOYHzZDRExzN41+El1EiSg55SZ1kZoxco58NIs1DENKqes601a3pqvTm0t7uCf9es45ooyPXzv99LVtlNI8WVKlZT4PEeGDDIANoqZgU43jmHM+moBKEW1qu9RENBHp98uF380Pf2h8+OPpwsfiwifi0kGMR1EV+ejNYikRJWIn2mfG3j1x7t75+c+Jpz2vedrT0tmIiMhHZTUMQymlbdumaWTVLaWUMp/Pj7/o7hQdPc7jHy0d1dQwDAsfRtuWEpMn3m0qpSglD8OSgqqUklLyWgPYFGoKNs84jkeTUSmlJlJfmpS6x8rldw4PvGP4+LvLJy/EQZeaNjVNpDbSTnS70T/pjypRHomDh2L/t/InxoM8Rj4X05d1d7+0uftl7bPOpt0o4xDjOI7DMLRte5RVS/7zsg6m0xhzaRf8l991n7VAeddGPykRMZ0uY62Urotr/3V3XXR9iSiTyQIfdMyRUpovcelCAE6RmoJNMgzD0WRRk1Kfuja1JfJb5ve/afy9d+WP903fR9Om5nzsnPCT/BSpjdRe+Y8SkaO8ffzYm4cPzyM/r73jq5rnfWX/vGm0OcYhj7NxTCn1fa+pbikpxcF++S//xM7e2QU+StvGZBLXpkvbxf/5z/p//o/6JZzp13UxmUT/2Y/+9753MpvF4k70KznO3xY/+COHi3oAABZMTcHGODg4KKU0qZmktkndA+XRf33w7rflj6YmJtGeb3abp3wVfYpoI7XRTlJbonwiX/rR8R0/On/Hl3T3fX334rub20sM88iz2axt275/8vkuttJkGmfPxe13LPVB2zZ2d2N3d6kPelXXRdfF7pkFPkTOsbMTEcUSGAAbSk3BBpjP58MwXJmP6j44PvT/zN/zW/lj02ZypumfekQ9qRSpj9SnJkd5+/jRXxvuf0F6+n89fdkfaO7KMQx5PDg40FTbbWkXTQHAhlJTsNaOLpGKiC61fWofHC/+0/lb31ce2knd2WZnQR31OE2kaXST1H44Hv2ug194cfPMPzf94qenvTHGIefDw0Mn/m2lozX9BNVSfGZFQ3fTBtgsagrW1zAM8/m8Sc0k2pSaH5+9598O7+2b9lyapKWfF5QiTaLtUvOB8vvftP/Tf7h7/n/Vv3Qa3SzG2WzWdd3RXaqWPCrYeE940XgdAWwQNQXrKOc8DEMexy41ferfWx74h/tv2U/znaZrY5WzQE2kabRdan5+/MCbx/v/5+mrX9Q8c4jh6MbBzvqDm/aEuaij5TpXMRQAbpqTc2AdzWaznHOfuj71Pz57z3fv//I85Z3oV5tSV7WRdqI/iOF7Dn7x/579VpfaSbQ558NDS5NtlaZp0hWrHst2S9da9WAAuAnmpmC9HF0olSIm0V2K2fcfvOn34lN7zaRdsyW/UkQfbZOanxs+8P7y0LdOvuxcTGcxHJ315zIqAOBW4B0PrJFSymw2S5Gm0d2fH/2mg5/+cDy6G926pdRVbaTd1H00P/ot+z/90XhsEl0qn55YW/XQAAAWTk3BuiilHBwctKmZpu53ykN/8/CNJZVpdMtfcOKmHLVfTuU7D37+vfnBPnVtagQVAHArUFOwFsZxPEqpSerePNz/tw5+qWnaPtpVj+uk+mibaL738Jd/dfiQoAIAbhFqCtbCbDZrjlIqf+gfzt6803T9pr08+2imqfvHs7e8Zbj/alCtelAAAAu0YW/XYPvknA8ODpqUpqn7nfGTP3Tw1jPNpNvM12YXzU7qf3D2G+8bH+yja1Kyyh8AsMU28h0bbJNhGCJiEt3946e++/AX+6bb0JQ60kXTp/b7Z79yf3xqEl1EzOfzUp5wSx0AgM23wW/aYAvM5/Occx/txTj8W7Nf7DfwBL8n6qNpIr1h/02XYt5Hm3Mex3HVgwIAOH0b/74NNtc4jsMwtNG0qfk7h786prz+y06UiBwlRylx3HRTH+2Q8vce/FKTmjaaYRgEFQCwfdQUrMx8Pm9T06fuX85/+4Pxqcnap1REDGU8n3ae096+X4Z8bFBNor2/PPpj83d3qWtTo6YAgO2jpmA15vN5RPTRvmv42E/N37/+95WKiDHKQcz/l73XfPvZr35t/zmXy3yM666BniJNU/eT8/e+b/h4V5o85qMrxAAAtoaaghW4eo5fSvFP5m/baSbt2qdUiZiV4XXTFz+7vT0i/se9V/03u190uQzD9YOqjTRN/T+a/0Yq0abmKCDZIDnncsWqx7LdyrVWPRgAboKaghWYz+dNSn1q/+XsnRfTbCMW8ZuX8fZm9xt2X3b1K183fcFfOfsVQ4yzMl7vDWAXzcWY/dj47j7aZMF0eKInfJCS0rp/tgLAVRvwHg62zDAMKaVJ6n6/XPrp4T9Oolv/t05j5MMYvnXvKx739Zd0d//AudfvNZPDMjzpuhQpYhLdvxt+5+F0eZK6iHABFRzP9BTABlFTsGzDMKRIKXU/dPgbfdNuwjl+5bCMX7/zhfe055/43Tua3b97/vXPaW+73roUbaQ2Nf/k8K1N6lIkV0/B8ZrGoRlgY9hlw1KVUlJKfTS/Nzz0vvLQZBNeg/OS72r2/uTOS663QRPpu8794S+fPPfgOjNUfTTvzg9+qPx+H00pJefrXmoFALBBNuCdHGyTw8PDFJFS96/n79pJG7GOX56X8dvOftUNt/yzZ77kC7pnPum3mkg7qfs3h+9OqWtScrIfALAd1BQszziOKaUu2geGT70zf3z9F58oUQ7K8KfOvOzpzZkbbvwfh4feNXz8et/tonl7/tjHxke6aMdxND21EUqx1txyeJ4BNtW6v5mDbXJ0yVCT2n8zvmfaTJq1n5ialfHZ7e2vm77whlvul+FvX/rFyfVn246mp35y/A9NapPpqQ3RNE26xqqHs8WS5xlgQ6kpWJ6ccxtNKeObhw/3a//qGyIPJf/lJ6zj96TecPGNuZQ+2mO26aP99eHDpYxtqKkNYJIEAG5o3d/PwdYYhqFpmi41vzx8sGmaNZ+YKlEOy/Cnz7zifLNzw43/xf7bPjI+OknHpVRENJEiyq+PH+6iDUulb4Jr794rrhbJmX4Am0pNwZIMw5AiItpfHH5vcuwczjqYlfEPtE/7munzb7jlW2b3v+nwAydcUWOS2l8YfzdSm1Jy6dSac8rZcniWATaamoLlaaK5kC+/Pz/Yrfc7qCFyTuVbzn7lDbd8NO//0P6bd1N/wqm2Lpr3jZ+8XA6c7AcAbAE1BctwNA/Tpea3xo/3Tb/OC6PnKAdl+LO7X3Im9Tfc+G9e/Pm2NCdfnDBF6qJ5x/hxJ/sBAFtATcEyzOfzlFJE8678wJqvPzEv44u7u75s8twbbvkPLv3qw3m/v9HlUo/Tp/adw8cjNSkll4gAABttrd/VwdbIOadIUdK7xwfbNX7dDZEjxTfv/aEbbvnzhx946/wj09Td7CxbG8078yciUjI3BQBsuPV9VwfbJKXURPpkeeyxOFjb1fxylIMy/592X33D6aYH8sUfvfz23dRV/FmaSI/GwcPlQhONual1Vp5g1SPaYp5ngE2lpmAZUkpdan43P9JFs6YtFTEr4xf1937x5Nk33PINF9/Yp7Zuki1FtNH8p/GRdl2rEmCtWAEV1pmagoUbhmEcxxjzR8dHmvRUX3QlyjzGwzLMyjjGqX2MPY/cp+Yv7X3ZDbf8vou/dCnPbvZyqWu1KX00P5pKKqU42Q/geG5XAOtMTcHClVJSSpGaj8XFpzIhk6McluFCOXxmc+7P773666aft19mY5zCZ5ZH5/j9xb3X3PDmNz9x8N53Dw9MUvtUju1tNB8tj0VKFqKAJ/KiANgg3aoHANvvyvRL+vD4aHvzHzGWKGOUWRlKSq/qn/1Hdz7/ue3TIuJL+mc/vdn9kf2376b+5GuUP9nPj1kZXts/9wu7Zx2/5QfGh37s4F1nqi6XulYT6SP5saNy88YRANhcagqWpvlEuXAmTW72t41R7m7Pf2X/uV89ff7jvvW10xfspu6HLv/Gbuqqg2qIcZq6b9x79fGbzcv4fRd/eaf2cqlrNZE+EZeOasr1AOvs2taVvQuSS7n2mfb5AsBmcaYfLE05jLHivr0l4o9NXvjElDry2snnfvvZr5rnPI+aC5CO7tX7v57gcqm/dfGNQxn7qL9c6qom0n6ZH/3a9QDc4rwAADaamoKFK6WkiIgxVS1B0UT8w8u/+puzj1xvgxd1d/2N819XoszKzQVViTgsw9dMn/+i7q7jt/zR/d/80PipyVNYeeIJD12iDMncFACwydQULFxKKUW6nGd1H0K30eyk/u9d/rUPjg9fb5vntLd/z7k/0qV2VoaTnyc0lHGvmf6Z3S8+frN3Dh//2cPfmaauYmLtelKkgzSc4g/k1D3uZlM5OwNtcdxvCmBTqSlYuFJKk9J+M1SnQxfNJDVvuPjGh/Pl621zZ3P2+87/0b1mOitDOcHK6WOUg5h/043O8btU5n//0v+3m/rTvT1Uirh85WQ/1lP6bM363iltC3zWU73qwQBwE9QUbIY+2ijpOy/87DGn851L07977vV3tWcPy3h8UB2t4/e66Yue1z79+Mf9mxd+rpR4KmsGHs8n8QDA5lJTsHBH5+7s5u4pdkOf2v0y/6sXfuqYbZqUvufcH3lue8dBGfL1g2pextub3W/Yffnxj/hPL73lwXzxFC+XuqpEVCxvyJJ99ql+undxnOkHsKnUFCzDmMfd1D/Fd0kpYpK6R/LBd134ueO3/M5zX/uS7u7rBdUY+TCGb937Q8f/kF+Z/adfmX/wdC+XuqpE2RmbXLLzmtbW4/9q/EUBwBOoKVi4tm1LRIo2ylNdvy5FTFP7ofGRv3PxV47f8lvO/qFX98/ZL/Pxs4OqRDks45/ceck97W3H/PYH86V/fumtO6l/ijfqvZ4UKZqnOlkHALBaagoW7uoi4JNoT7I+xPFSpGnq3jl87Ef23378lt+49+rX7bxov8zG+EzFzUu+q9n7+p0vPP73fvfFN3apXdDlUiXKjluHAwCbT03BwjXN0Qut3NOeP+Zappv4gZF2Uv/Gww/8+4P3Hb/ln9p56Z/efcXlMh8ix9E5fmX4y2e/8vjf9QMXf/mxfNgv4HKpIznK3c3ZiBIRbbuoR+EpciswALghNQULl1IqpUQp98S58TRqKiKaSLup+1cHv/3Ls987fsuvnb7gG8+8ar8MszIelOEbzrz8Gc3eMdv/1OF/eOfw8UlqF3eZzBjlvnT+yvIcrC/3m1oWq1AAbCo1BQt3NDdVUrkv3XYqc1NH2mh2U//PLr/lfcMnjt/ytZPP/fazX7Vf5nc35183feExW354/NS/2n/HTuoWdLnUkTHKPen80dzUlYk71p1FKADgibyPgYVrmqaUMuTxmbF37SVMT10XzTR133fplx7IF47f8kXdXd9x7mu+ee/Lj9lmLPm7L75pmrp2wXuGMfI9cXbIYylFTQEAm8v7GFi4o2AYozwvPW04vbmpI300TWn++oWfu1gOj9/yhd1dd7Vnj9nguy+96bAMXSz2QqYSMZb8B9qnH4WlFdLX2WffbsplVIuTrzmjMjvZD2CDqClYhqZpcuS7m/Pn8uQUT/Y7MkntWPJfvfD/Dk/h/e6/Pnjn7w4PTRd5udSRHOW2ZufpcTaHial1l67RJH9Zi9N85nn2ogDYKPbasAxt2445lzy8pLlzONWT/Y70qb2cZ99x8Wfqfvv7h0/+u4P3TlO/iBv1Ps4Y+aVxd+RhzLnrrJMOcAOmK2GdqSlYhqZpUkpjipc1z5ovoKZSxCS1D4wXvufiL9zs790vw/df+qVp6tqlLDQwj/ELm7vGVFJKlkdfZ0drUTrTbyk+60w/p7/yOEdHkGOseoBwS1NTsAx930fEEOPLurvneXjq9/B9oqO7+r5/eOgHL735pn7jGy6+MZfSL/hyqSMlyhDlZenuowk6JzWtsyfcb8p7tsX5zDPrWQbYLN7KwJI0TTPkfNs4/fz2Gae+FsWnHyLSTurePP/Q/7X/Wyf8Lf9i/20fGR+dLOxGvY8zRH5RevrZmAwlSykAYNN5NwNL0vd9iTKP8SvT585jXNCjNJF2Uv+zh+//hcMP3HDjt8zuf9PhB3ZSt4TLpY7MIn9F+twhcinFRVNrTu4CwA05WMKSdF2XUhpi/NLm3pzzqa/sd1UbaTf1P7z/tt+cffSYzT6V9//x/pt3U7/QG/VeK0dJkb60vW+IMa6c/QgAsLnUFCxJSqnv+yGPXW5e2dy7iLUormqj2Un937v8qx8cH77eNm+4+MamNN0SdwLzyK9K9zQ5DXm0/gQAsAXUFCxP27aR0jzlr29efFjmi5ueiogumklq3nDxjQ/ny0/87j+49KsP5cv9si6XiogcZRbD65vPH1KOlKbT6dIemmrls1jTb1HKZ6/pt+rhAHAT1BQsT9d1TdMMke9N576g3LWIG09dq482Svr2Cz+zX4Zrv/7zhx946/wj09Qtc+2wIfIXNffcl24bIjdN45ocAGALuAoclmoymRwcHBym8b/ovuA785v6aBa6AsQktYdl+GsXfvoHzr/+6CsP5Is/evntu023tMulIiJHOYjhT6QXzmLMpeyYmNoEOedr7xk6DmU+j9nhCke0hXKOrkvjWK4+1e7TCrBZ1BQsVd/3s9lsnofnxu0vjDs/GI9MFnyjp0nqHskHf/3Cz/2Nc18XEW+4+MY+te1y56Xnkb8wPfN5cfulPGvaxmp+m+LqjUFTitSk137FfGfXe/3TVEramZZxaK7eZsr9pgA2i/c0sGx93x8cHBym4b9v/uC3DT/XNk27yGmiFDFN7f3jIz94+dcPy3Apz6ZpqS/8McoY+c80Lz+MIUc5Y2JqA5USKcX/8BcuWT3klJUYxpjPFRTAplJTsGyTyWQ2m81zviud+aPt83+m/KedWOwlTCnSNHXvmH8sIiapXeYbtxIxi+F16fPujDOXY940jdX8Nsi1Z53N5zGfr3As2+0zz7Mz/QA2i5qCFdjb27tw4cJhjP9ZvPjX4yOXY+gXfOpdEymWeKHUVUPkc2nn65sXHcZQSjlz5szyx0A17+yXz3MOsFnUFKxASqnrunEYckr/Xbz8B/KvNWm60PP9VmKMchjjX0qvypHHkieTiaX8NkjXdbfddtuqR3HLKaW4dApgg6gpWI3d3d2LFy/OIr80PfN1zef9TPndnegWur7fkpUohzH88fR5L053Xo7BPaY2jvQFgBtysITVSCnt7OyMedwv8/88vuBzyu2zBd9+aslmkZ8Tt/3J9Pn7ZT7mcXd3d9UjAgA4ZWoKVqbv++l0OpQ8i/lfTF/clZhvS1DNY5xE+03p1fMoQ8nT6dTiEwDA9lFTsEo7Oztt285LPpd2vi29NkcZNj+ohshj5G9uvvRM6udlbJrGOX4AwFZSU7BiZ86ciRSzGO5Lt31zvPpwnG90UA2RZ5G/qXnN58RtszKmJp09e3bVgwIAWAg1BSvWNM3e3l6JOIzhBenpf6774v0y29CgGiIfxPiNzRe9MJ5xGENJsbe3t+pBAQAsipqC1Wvbdm9vbxjHwxheFfd+Y/PKgw2coRoiH8b4Z9MffGXcexjDMI5nzpyx1jMAsMXUFKyFo6Caj8PlPHtleda3Na8tUTZoUYp5jEOM35y+9DVx336ZD3nc29uz8gQAsN3UFKyLyWRy7ty5kuIghuenp/2VeG1TymGMJcqqh3acEuUwxi7a70hf8cJ4+kHMc5QzZ870fb/qoQEALJaagjXSdd3Zs2fHnA9juDfOf098zX3l7H4M47oGVY5yEOO9cfb/SF91T5w7jKFE7O3tSSkA4FagpmC9tG17/vz5EnGQZ9No/1p8+evLCy6X+TzyWhVViZhHvhzzr4rnfkd8+ZnSHuRZiTh79qwT/ACAW0S36gEAj9e27W233XbhwoX5OJYUfzw+/wXxtH8W77gQs0m0bax+XYcxyizG8zH539KrXhRPP4hxKLntur29PctOAAC3DjUFa+rcuXOHh4f7+/s5zT8vnv63y9f++/idn8jvn7RdH01aUVMd3V94iPLV8dxvSC8pUS7HPJfS9/2ZM2dWMiQAgFVRU7C+ptNp27YXLlzYj9yl5o/F531pe98Pl99+X/r9abRdNM0Sm+pojcHDGF8Sd/638bJnxO5hGYaSm6Zxdh8AcGtSU7DWuq67/fbbDw8PL1++PKZyW0z/crzmI+WxH4/3vy0+Nk19v/imyp/uqOHFceefihd9btxxGMPlMi9RptPpzs6Os/sAgFuTmoJ1l1La2dnp+/7SpUuzcRxSvjPO/IV45cfiwk/k97+9eSBF9NF2kU739L8SZYgyj7FEvCrufX284J44dxjjpZjlUpqmOXf2XNNYyQYAuHWpKdgMR2v9HR4eHhwczPI4j/yMtPuN6RVjKW8tH/2V9OH3xoN96rpo2miaqJwtKhE5yhh5HnmM8oJ42qvj3i+LZ0+jncV4qcxylKZpzuzuTiaTU/4TAgBsGjUFm2Q6nU6n02EYLl++PBvHeeQmpVeme14Tz340Dt9VHnxPfPI96cHH4rCNpo10VFZNxPWmrUqUHDFGzlHGKEPk8zF5WTzrC+LOl8Zd52M6i3GIfKnMS5S2bc/u7nad/QYAQISagk3Udd358+eHYdjf35/P52PKhzFOUvMlcc9r476uNB+LCx+Kx+6fP/yJdv/j6eIn4tJhm1McFdVRVpUSUSLOlPYZ+cyzyt696fy9zW2fE+fviXPDp+em8oUyK1FKKX3f7+7uWmoCAOBaago2Vdd1586dK6XMZrPZbDafz4eUDyOalM7H9A+mu78g39GWphIEsdkAACAASURBVInUlJQi7cdwGMNhO0bEdGyn0e1GV6LkVMZSuklfIsbIj8VhLiUijiJqMpk4qQ8A4EmpKdhsKaWj0/9KKeM4DsNwlFazGGZ5dnW1vaNfpIidsY2IEuUg5gcxL6VEiVJKP45ttKWUpmmm02nf987oAwA4nndLsCVSSl3XdV23s7MTETnnS5cuzefzUkrOOSKu/KIcbd80zVFiHf3izJkzfd87lw8A4OTUFGynpmkmk8l0Oo0rE1PXU0qJiK7rLHcOAHBT1BRsraNMuvYXJ9kYAIAT8lE0AABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFALCpUkqrHgLc0rpVDwBYlJzzCY+ypZS2bRc9HgDqjON4vf15KWUcR/twWBVzUwAAa80EFKwtNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAECNbtUDAFig/f39UsqqRwEsSUppd3d31aMAbiFqCthmDz300Hw+TymteiDAwpVS+r5/9rOfveqBALcQNQVss6ZpmqZRU3ArKKU0jUsYgKWy0wEAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAa3aoHALBA4ziO47jqUQBL0jQ+JgaWSk0B2+w5z3nOqocAAGwtH+EAAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ03BdhrHcdVDAOAUlFJWPQTgutQUAABADTUFAABQQ00BAADU6FY9AGBRUkoppRNuuejBAFDNXhrWlrkpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpqCrZVSWvUQAFgsu3pYrW7VAwAWZRzHEx5lSyldZ28AsKaO2Z+XUvq+X/J4gKvMTcF28mklwNY4fpduhw8rpKYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGp0qx4AsCillJNvefKNAVimUkrOOaV0ve8ueTzAtcxNAQCsteulFLByagoAAKCGM/2AbfbRj350Npv5WHfNlVL29vbuuuuuVQ9kLfzwJ3/7w/uPNf7R3rxcyl2TM3/+7leseiCnr5RiPwbrSU3B1kopnfzou63H6aO3INv6p9sa3ileq5RomtQlJ4/ctLHkvOoxLEJKqWn8e4A1paaAbeY9+kbw13StFClFeEYqpEjJMwcsl486AAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBrdqgcAsEA555xzSmnVA+E4pZRSyqpHsS7mZTzM45DyqgeyeXKUeRlXPQrg1qKmgG12xx135Oxd6Qbo+37VQ1gXrz1336W9eRM+ArhpOcpO2656FMCtRU0B2+zs2bOrHgLcnBefuXPVQwDgpFw3BQAAUMPcFGytk1+I4pIVgHV2zPWfduCwWuamAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCG+00B2+zBBx+cz+fXu08LsE1KKX3f33XXXaseCHALUVPANpvNZmoKbhGlFLeyBZZMTQHbLF2x6oEAy+DFDiyZ66YAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgBYa+6jBWvL3Xtha5VSTn1LAJaslJJzvl5Q2YHDapmbAiJ88AkAcPPMTQHbrJTig1u4RXi9A8unpoCI7T1XZG9vbxzHVY8CWJK2bVc9BODWoqZga6WUnL93xx13rHoIAE9V07g0A9aUFycAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABAjW7VAwBYoIcffngYhpTSqgcCLFwppeu6pz3taaseCHALUVPANrt8+fJ8PldTcCsopfR9r6aAZVJTwDZLV6x6IMAyeLEDS+a6KQAAgBpqCgAAoIaaAgAAqOG6KdhapZSTb3nyjQFYppxzzvl6l4TZe8NqmZsCAFhfltKBdaamAAAAaqgpAIANZuYKVkhNAQAA1FBTAAAANdQUAABADSukA9vM4u9w6/B6B5ZPTQHbbDqdtm276lEAS9J13tgAS2WnA2yzO++8c9VDAAC2luumAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGp0qx4AsCillFPfEoDlyzmnlJ70W3bgsFrmpgAAAGqoKQAAgBrO9IPtlK5Y9UAAeEpSSk3j429YU16cAABrzcVRsLbUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQCstZTSqocAPLlu1QMAFqWUcvItT74xAMtUSsk5Xy+o7L1htcxNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1OhWPQAANtj73hPf9W2xe2bV49hM+5fjDX83nv+CVY8DgFpqCoB6/SR2z8S586sex2YqJfp+1YMA4Clwph8A9dKqB7DRUvIEAmw2NQUAAFBDTQEAANRw3RRsrZxz05zoE5NSyqIHA0C1Y/bSOedljgR4HHNTsLVSckUGwJazq4fVUlOwtcw4AWw9u3pYLWf6wdZqmubkn1n6dBNgbaWUrreXPuEZ3cCCeAUCAADUUFMAAAA11BQAAEANNQUAAFDDKhQA1JvP42A/XAZfZ38/5vNVDwKAp0BNAVDvvmfHX/jmmExWPY7NdHgYz7pn1YMA4ClQUwDUO3suvvwrVz0IuLXlnNu2XfUo4Bbl5AwAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAIC1llJa9RCAJ9etegDAouScm+ZEn5iUUhY9GADq5JxzztcLKjtwWC1zU7C1buqzTB98Aqwn+2dYZ2oKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKBGt+oBAAuRc845N82JPjEppeScFz0kAOqUUiq+BSyBuSnYWimlVQ8BAGCbqSkAAIAazvSDrZVSMj0FsOnSFaseCPAkzE0BAADUUFMAAAA11BQAAEANNQUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAANdQUAABADTUFAABQQ00BAADUUFMAAAA11BQAAEANNQUAAFCjW/UAgEUppZz6lgAsWSnlmL20HTislrkpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBpqCgAAoIaaAgAAqKGmAAAAaqgpAACAGmoKAACghpoCAACooaYAAABqqCkAAIAa3aoHACxESiml1DQn+sQk55xSWvSQAAC2jJpakkceecS7VZYmpVRKeeyxx05eU13X9X2/6IEBUKGUcr1v5ZyXORLgcdTUkpx8lgBOS9M0J2z4o4msRY8HgArpiif9btM03mDACqkpAID1tb+/f+HChesl09Hc1NEpCcsdF2utlHLHHXesehS3BDUFW+vkM07mpgDWWdu219tLp2sseVSsM6eALo2pYQAAgBpqCraWsz4AtsMx+3O7elgtZ/qtgJXTWIJxHLuuO/mZfi5iBlhPXdcdsz9vmqZt27Ztlzwq1tN8Pl/1EG45amo1fJIEAJwW7yuICNfOrYRPowEAAGqoKQAAgBpqCgAAoIbrppaklHJ0TrMzmwEAYDuYmwIAAKhhbgoAALZBzvloZT8nQy2NuSkAAIAaagoAAKCGmgIAAKihpgAAAGqoKQAAgBrW9IOtdfL1fK7eDw2AdZNzPmYXbe8Nq6WmliSldLRg5dH/AwCchHcOsM6c6QcAAFDD3BQAAGyDpvn0TIkpzaUxNwUAAFBDTQEAANRQUwAAADXUFAAAQA01BQAAUENNAQAA1FBTAAAbrJSy6iHArUtNAQBsNjcXglVRUwAAADXUFAAAQA01BQAAUKNb9QBuFaWUo4tEXSpaYeWng/tbA+AW5PgLN6SmWHfjOI7juNoxtG3btu1qxwAAy+T4Cyehplh3KaWVfza28gEAwJI5/m6inPPRk2Zab2nUFBsgpei6aLvSLPdCv5xjHNIwLPVBAWBNOP7CDakpNkBK+e1vnb7z7ZO2XeoHLeOYXvqK2cu/6DDCaQYA3HIcf+GG1BQboGmi78vObun7EkuZ9z+aH5/Po+9L08SGzpbnnJuTfZzofACAdXbMXnqhO3DHX7ghNcXmWdreHAA2wnIOW46/8ERqis1TcgxjjEPK+ZR/ctNE25VuW04raJrmhEc+l/kCrLPjF4RY2j7c8ReeSE2xeXJJX/Sq4WUvX8jVqb/9ju4db+3EBQA8juMvPJGaYvPkHPNZzIc0n53yT+4nMZ9FzuHmFgDwOI6/8ETLXfASAABgW6gpAACAGs7049Qs6CrYJ/7YdOWrp/6IKcqT/tDF3Q/e4kUAPEULOkg5/sJJqClOx2w2W9COKaXU95/1lTGP8/n84HB26udtjznm8zLmaD77vO35fL64P91kMlnETwbgVjCfzw8ODtq2PfXjlOMvnISa4hRc+9FR00TblraLk9029iRK15XptPR96a78g92ZljNnyjA55c+ruq7sTEvfx5UHKtNp2T1T+tNbvijnGIcYx08vL3v01PmEDIAKRwffo/9v2+T4ewzHXxZETXHKUhO/+bbpO98+abtT20O1bTzySPPoI83RUj/jGA880L7tzdNxPK1HWN4DjUN66StmL3/FLE77Zh0A3Mocf4/n+MuCqClOWZOi78vObun7U9ubN01cvlz6vhx93tY0MZ2W3d2yiLsHLvqB5vPo+9KkOO0jEQC3rpRSk4rj7zEcf1kQNcXpyFd2eOUap/fDI49lHCPnEhGlpDyWnE9/fn4JD1TK0VOUS0nhKlgAnpqjA27O2fH3eI6/LIiaYjM8485851356OKsUj79v41+IABYf46/cDw1xQZY2i7VvhsArnL8hRtSU5yaqysLXbHqAa2lK09Oc+XjN0cPAJ6SlFLTNCkVx99jOP6yIKe2iCZcYfd0Ep4lAE5NKcWR5WQ8S5wyc1NLcvWqUJ+FAADAdjA3BQAAUMPcFGynm1ok15QpwDo7Zi9tB861rv578A9jadQUbKGbvQY5pXT1XiWnMoB8zQ0X27bd6H16SmkcP3Ozx6bZwin9UsqVVWTKfEjzeWzjn/IU5Bx9H31Xrt6vZiuv9///27ubHMuN5ADASb6uKkgeoIXWCSRBsGB7YdjwpWY3J/HOJ7MNeCFtdAC1Vj31Q9ILqtilVnU9Voovmcz4vpVGCw1fVgSTkRlJLvl79ORNTeRv13VH/ytA21RTQEopvX///pdfftlkzu667u7ubv7naZq++uqrQz8KdF3366+/Lg/NV1dXh/45f9R13e3t7fwDT6f0v//95v/+5/p02vuyqjQM6R//+e6f/uVhfj6fpunm5qa9eLi/v08pTdP09u3bo/+6ruvev3+/5O/19fXhfpH35ELlVFPAx7fbb/Vf6/t+/q+N43g6nZ5uVR1O3/en02kZnAZW6z/Rdd3yA99cpZub0xdfnt68aeo3buXhobu5Ob25+thI02Q8zAk7/7pDJ29KqX+UGv17Abs73pY3AABADexNsTndCGsYJWokLtcwSlRIK+BqBoqNqabYxtN3yCz2vaQ6TdM8RONyin3vK4KPcThO0zSlaRwF5rOmcZqmNMpfajJPuMubhMy/n2P+5UJUU4U4Qkooy4uzjvgGLVLXpeQ54zlu4wD8ngcdAACAHPam2IadtzzGjRr0fa/pJYOt10NYXjHa6v221d91aa2O29IM1eoPrJBqis3J3jWMEjXqROYKRokKeXRezUCxMdUU23j6FgoH2F82PjkFCzVYvik0jtPoxNSLxjQf93eKnVo8fQuF+fdl5l8uQTXFZpad5d4K2Yv67uMgeRqjEnP+9n3XW7h9UZ/mj1OnJH+pxuM30yfz78vMv1yCnm8AAIAc9qbYzLzM40sXZ02X7zRYGj926aTvum7pHJsvY/mfB/V0JOd2mn2vZ1td181/ppRS36dh6qfp5A3pz5qmbpiGYRiGIaXHUG8vHuZgaCN5U5j8nf+hsV+3uQLzLwGppqBNV1dXX3/99V7nku/u7ub/63Ec3759u8s1bKjv+/ntbdM03dzcNPa80nXdhw8f5n++upr+8uX11enm6tTUb9zM2P3ly9t37+7u73/LrC+++KK9eLi9vZ1rqgaSN/0+f6+vr/e+nI3N+auOgh2pptjGNE1eKJThcvPfslV4of/+C+bnsKWaKn8BmxuGYRnPJte2h2F4/HtN0zROaZzsTT1nSt3jhs1vb6FoOB7aSN705C40Pdr3erb1NH95lcYigR05NwUAAJDD3hSbeboZ0t7631bmUTE81Eb+riF/qZP8XUP+ciH2ptiGNoM8xo0aiMM8xo0aiMM8xo2tqKbYhqWePMaNGojDPMaNGojDPMaNrej0K2TZeZe9RLDEuYAHABpmb4pteGjOY9yogTjMY9wOYXpi72u5iFZ/16W1Om7NB3yF7E2xjfm7gcn51xWe3uX0bVMD+bue/KU28nc9+csl2JtiG+7geYwbNRCHeYwbNRCHeYwbW7E3xWY+ft7UHepFo7Ux6iN/V5K/VEj+riR/uQTVFJuZb0xdl3p3qBf13W/2vhD4SP6uJH+pkPxdSf5yCTr92IYd8zzGjRqIwzzGjRqIwzzGja3Ym2IzTsGu5F07VEj+riR/qZD8XUn+cgn2ptjGJzcnm+if88nIuKlTA/m7kvylQvJ3JfnLhaim2IYu5DzGjRqIwzzGjRqIwzzGja2optiGNZ48xo0aiMM8xo0aiMM8xo2tODfFZpa+7fT4EtK9r6hG45TSb63be18KPCF/15C/1En+riF/uRB7U2xjHMe9L+GQjBs1EId5jBs1EId5jBtbUU0BAADkUE2xjb4XSzmMGzUQh3mMGzUQh3mMG1sRSQAAADm8hYJtLIc6fRjvLF8PpDbydz35S23k73ryl0uwN8UG/vjRBh9x+Jw/joxPXrAv+bue/KU28nc9+cuF2JtiG+M4znelednHG1o/Z5x+tzZmlA5h+YO1+veSvysFyd+2o7098nelIPlLeaoptrGc5uy61HVd31nxeV7ffZQsjFEH+buS/KVC8ncl+cuF6PQDAADIYW+Kbeg0WEmnARWSvyvJXyokf1eSv1yIaqqQ5s9dfLJjbvv8cz4ZmVY7DZ6+Y2rfK2EN+btSkPzlWOTvSvKXC1FNsbFxTPf33d//3p3u3aeeMQzp/r4bx72vA54jf18mf6mZ/H2Z/OVCVFNsbJq6f/23D//+Hx+63qbEM6axG4Y0DKY6aiR/XyZ/j6j53pCF/H1ZkPzVG1KeaoptLH3bw5CGYf53jd+w/hw3Oyoif19J/lIR+ftK8peNqabYwPT0XCevZNzYl/z9M4wb+5K/f4ZxYxOqKbbx7t07BzozuJUfwvJ9klbJ3zyt5u/Tb/JQP/mbp9X8pTzVFBsI0pIOTZK/cFzyF3bn670AAAA5VFMAAAA5dPoVogedUJaAF/YAQMNUUwBnxPleDaTmjuJYzSQUq5nl6fQDAADIoZoCAADIoZoCAADI4dxUIc5dEMoS520EvPwlFNEOsJ5qCgBoltUQQmlsNfMQVFMAZ3gnGKF0j/a+EIADcG4KAAAgh72pQnQaEEpjnQbyl1BEO8B6qqlCdE0Qiq8HApWwGkIoja1mHoJqCgBoltVMQrGaWZ5zUwAAADnsTRWi04BQGus0kL+EItoB1lNNAQDNshpCKI2tZh6CagrgDOcuCMX3pgDWc24KAAAgh72pQsZxnNf5bLwSwRLn4zjueyWb0ClEKKIdYD3VVCF93+uaII6lTajvbYADe7KaSSiNrWYegmoKAGiW1UxCsZpZnoEGAADIYW+qEOcuCMUbWgGACFRTAGc4d0Eoc5w3c+jCaiahWM0sTzUFcIZzF4Qyn7tw6AJgDfdKAACAHPamCtEpRCje0AoARKCaKkSnEKE09oZWqyGE0ti5KflLKFYzy1NNAZxhKYSAmgl7q5mE0thq5iEYaAAAgBz2pgrRaUAoOg0AgAhUU4VoMyCmNiJ/WQ1RHBLBOI593zcT7VYzCcVqZnmqKYAzlnMX2tCJYA74ZqK9jTUdeC2RX0wj90oAAIDC7E0VolOIUOZOoSTgAYCmqaYKidwpdHqT3pzS6aRhPQ1D9zCk4WHv67i8xgI+8rkL+buIk7+tfm+qmV+0nvxdxMlfq5nlqaa4uDen9F//eXNzs/d1VOD2Nv31b7cR7uY0Q/4u5O9BNba48yrydxEnfyMH/F5UU1zc6TTd3KQv/8HaWEqpO52mlBwM5TDk7xPyl4ORv0/IXy5FNVVI5E4hAvKGVgAgAtUUwBnTNDV2kgReMJ+7aGbtz2omoVjNLE81BXBG13Xa0IljPnfhYzUAa3gyAAAAyGFvqhCdQoSyvKFVaw0A0DDVVCE6hQhleUNrG81CVkMIpbFzU/KXUKxmlqeaAlhrXhYxRdGw9k5MWc0klMZWMw/BnQUAACCHvalCdBoQik4DACAC1VRpATuFhqG7vU0+QJ5Sur1Nw9D+OLTXKRR5NUT+LoLkb3JuqiHydxEtf5PVzIJUU1zcw5D++rfb00lWp2HoHoa9LwJeQ/4u5O/RBVzNlL+LIPnb3mrmIaimuLjhIQ0PydoYxxX5FLv8DcjXe5shf6EA1VQhkTsNCEinAQAQgWqqkMhr2wTU2BtarYYQinNTcFxWM8tTTQEAzbKaSSiNrWYegjsLAABADntTheg0IBSdBkAlzL+EYv4tz94UAABADntTAGcsa9uW+ohgerT3hQAcgGoqtK7rTqfTcmCRg5qmaRzHYRg8/VzIcoq9qkyRv22oMH+7R3tfSMvkbxsqzF/KU00VUufadtd1P/74408//eRNR4c2juO33377zTff1BNddQZ8Y+RvGyrM38bUeTuSv22oMH/rDPi2qaYKqXNtu+/70+l0fX19Op32vhbyDcMwr3HWc8a6zoBvjPxtQ4X5SwHytw3yl6SaAjjLUh+hNHZuyuIOoQj48uwvAwAA5LA3VYi1bUIR8EAl3I4IRcCXZ28KAAAgh70pgDMs9RFKY+emAC5KNQVwhkO9hOJ7UwDrqaYKsbZNKAIeqITbEaEI+PJUU4VY2yYUAQ8ARKCaAjjDUh+hNHZuyuIOoQj48lRToY3jOAzD3d1d33u744HNf0cfYo9G/rZB/sYkf9sgf0mqqWLqXNuepum77777/vvvLWAc2jRN8w197wv5qM6Ab4z8bUOF+duYOm9H8rcNFeZvnQHfNtVUaNM0PTw87H0VQA75C8clf6EZqimAMyz1EUpj56YALko1BXCGQ72E4ntTAOuppgqxtk0oAh6ohNsRoQj48lRThVjbJhQBDwBEoJoCOMNSH6E0dm7K4g6hCPjyfOUAAAAgh72pQqxtE4qAByrhdkQoAr48e1MAAAA5VFMAAAA5VFMAAAA5nJsCOMMrkgjF13sB1lNNFeJpjFAEPAAQgWoK4IxhGOaycBzHva8FLm6O82EY9r6QbVjcIRQBX55zUwBnmJMISNgDrGFvqhBr24SyxHkzy9vAQZl/CcX8W569qUIs8hGTyAf25S5ETCK/GNUUAABADtUUwBnjOE6P9r4WuLg51PXFAazh3FQh4zj2fZ9S8jRGBEvh0cYDWd/3XpFEHPM7weY5qwHmX0JpbP49BNVUIZ7GCGV5Q2szD2TAQZl/CcX8W56BBgAAyKGaAjjDuSlCcW4KYD2dfoXo2yaUxvq2NQgRUDNhb/4llMbm30OwN1VIM9MSvIrIB/blLkRMIr8Y1RQAAEAO1RTAGcu5KY0TRDAHvGgHWMO5qUKWvm3zExGM4zj3GLQR8Msblr1wlgjmgG8m2s2/hNLY/HsIqqlCPI0RioAHKuF2RCgCvjwDDQAAkEM1BXCG700RilOCAOvp9CvE9y4IxfcugEqYfwnF/FuevSkAAIAcqikAAIAcOv0AztA4QSjzG5b1xQGsoZoqxNMYoSzfu2jjgazrOi+cJY75DctzzDfA/Esojc2/h6CaKsTTGKEs37to5oEMOCjzL6GYf8tzZwEAAMhhbwrgDJ1ChOLcFMB6gWuajgAAA5NJREFUqqlCPI0RSqt92y0dJoFntRfk5l9CaXX+rZlOv9Lam6jgE4IcqJBbE80T5LtQTQEAAOTQ6Qdwhk4hQnFuCmA91VQhnsYIpbG+bW9YJhTfm4Ljamz+PQTVVCGexgjF9y6ASph/CcX8W547CwAAQA57UwBn6BQiFOemANZTTRXiaYxQ9G0DlTD/Eor5tzydfgAAADlUUwAAADlUUwAAADmcmypk6dvWxkoEjQV8Yz8HXjY92vtCtiF/CUXAl6eaKmT53oXX/xNBYwHf2M+Bl3WP9r6QbchfQhHw5en0AwAAyKGaAgAAyKHTrxBtrITSWMA39nPgZc5NwXEJ+PLsTQEAAORQTQEAAORQTQEAAORwbqqQpz3ofa+IpWVzhLfUty1/iWPJ3zaSN8lfImlv/j0E1VQhy+v/+77/+eefhTgN67qu7/uWvnchf4ljyd82kjfJXyJpb/49BNVUaS1NURCN/IXjkr/AJdjyBgAAyGFvqpCWetBhvTbCXv4SUDMxL3+JSdgXo5oqZBiGvS8BdtBG5LfxK+BVmgn7Zn4IvIrIL0Y1VcgPP/yw9yUAmeQvHJf8BS7KuSkAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAcqikAAIAc/w+eXtfCEtWdtgAAAABJRU5ErkJggg==",
        "properties": {
          "description": "Job Vacancy",
          "numberOfJobsToShow": 3,
          "showAllJobs": true,
          "showBoxShadow": true,
          "showDescription": true,
          "enableBackground": true,
          "currentBgColor": "#3D6187"
        }
      }
    ],
    "loaderConfig": {
      "entryModuleId": "fph-job-vacancy-web-part",
      "internalModuleBaseUrls": [
        "https://localhost:4321/"
      ],
      "scriptResources": {
        "fph-job-vacancy-web-part": {
          "type": "path",
          "path": "dist/fph-job-vacancy-web-part.js"
        },
        "sp-client-custom-fields/strings": {
          "defaultPath": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
          "type": "localizedPath",
          "paths": {
            "ar-AE": "node_modules/sp-client-custom-fields/lib/loc/ar-ae.js",
            "ar-DZ": "node_modules/sp-client-custom-fields/lib/loc/ar-dz.js",
            "ar-EG": "node_modules/sp-client-custom-fields/lib/loc/ar-eg.js",
            "ar-MA": "node_modules/sp-client-custom-fields/lib/loc/ar-ma.js",
            "ar-QA": "node_modules/sp-client-custom-fields/lib/loc/ar-qa.js",
            "ar-SA": "node_modules/sp-client-custom-fields/lib/loc/ar-sa.js",
            "ar": "node_modules/sp-client-custom-fields/lib/loc/ar-sa.js",
            "tzm-Latn": "node_modules/sp-client-custom-fields/lib/loc/ar-sa.js",
            "ku": "node_modules/sp-client-custom-fields/lib/loc/ar-sa.js",
            "syr": "node_modules/sp-client-custom-fields/lib/loc/ar-sa.js",
            "ar-TN": "node_modules/sp-client-custom-fields/lib/loc/ar-tn.js",
            "da-DK": "node_modules/sp-client-custom-fields/lib/loc/da-dk.js",
            "fo": "node_modules/sp-client-custom-fields/lib/loc/da-dk.js",
            "kl": "node_modules/sp-client-custom-fields/lib/loc/da-dk.js",
            "de-AT": "node_modules/sp-client-custom-fields/lib/loc/de-at.js",
            "de-CH": "node_modules/sp-client-custom-fields/lib/loc/de-ch.js",
            "de-DE": "node_modules/sp-client-custom-fields/lib/loc/de-de.js",
            "de": "node_modules/sp-client-custom-fields/lib/loc/de-de.js",
            "dsb": "node_modules/sp-client-custom-fields/lib/loc/de-de.js",
            "rm": "node_modules/sp-client-custom-fields/lib/loc/de-de.js",
            "hsb": "node_modules/sp-client-custom-fields/lib/loc/de-de.js",
            "de-LI": "node_modules/sp-client-custom-fields/lib/loc/de-li.js",
            "de-LU": "node_modules/sp-client-custom-fields/lib/loc/de-lu.js",
            "el-GR": "node_modules/sp-client-custom-fields/lib/loc/el-gr.js",
            "el": "node_modules/sp-client-custom-fields/lib/loc/el-gr.js",
            "en-AU": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-SG": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-HK": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-MY": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-PH": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-TT": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-AZ": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-BH": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-BN": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-ID": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "mi": "node_modules/sp-client-custom-fields/lib/loc/en-au.js",
            "en-CA": "node_modules/sp-client-custom-fields/lib/loc/en-ca.js",
            "en-GB": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "sq": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "am": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "hy": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "mk": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "bs": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "my": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "dz": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-CY": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-EG": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-IL": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-IS": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-JO": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-KE": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-KW": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-MK": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-MT": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-PK": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-QA": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-SA": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-LK": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-AE": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-VN": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "is": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "km": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "kh": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "mt": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "fa": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "gd": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "sr-Cyrl-BA": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "sr-Latn-BA": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "sd": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "si": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "so": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "ti-ET": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "uz": "node_modules/sp-client-custom-fields/lib/loc/en-gb.js",
            "en-IE": "node_modules/sp-client-custom-fields/lib/loc/en-ie.js",
            "en-NZ": "node_modules/sp-client-custom-fields/lib/loc/en-nz.js",
            "en-US": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "bn": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "chr": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "dv": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "div": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "en": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "fil": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "haw": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "iu": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "lo": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "moh": "node_modules/sp-client-custom-fields/lib/loc/en-us.js",
            "es-AR": "node_modules/sp-client-custom-fields/lib/loc/es-ar.js",
            "es-CL": "node_modules/sp-client-custom-fields/lib/loc/es-cl.js",
            "es-ES": "node_modules/sp-client-custom-fields/lib/loc/es-es.js",
            "gn": "node_modules/sp-client-custom-fields/lib/loc/es-es.js",
            "quz": "node_modules/sp-client-custom-fields/lib/loc/es-es.js",
            "es": "node_modules/sp-client-custom-fields/lib/loc/es-es.js",
            "ca-ES-valencia": "node_modules/sp-client-custom-fields/lib/loc/es-es.js",
            "es-MX": "node_modules/sp-client-custom-fields/lib/loc/es-mx.js",
            "es-PE": "node_modules/sp-client-custom-fields/lib/loc/es-pe.js",
            "es-UY": "node_modules/sp-client-custom-fields/lib/loc/es-uy.js",
            "fi-FI": "node_modules/sp-client-custom-fields/lib/loc/fi-fi.js",
            "sms": "node_modules/sp-client-custom-fields/lib/loc/fi-fi.js",
            "se-FI": "node_modules/sp-client-custom-fields/lib/loc/fi-fi.js",
            "se-Latn-FI": "node_modules/sp-client-custom-fields/lib/loc/fi-fi.js",
            "fr-BE": "node_modules/sp-client-custom-fields/lib/loc/fr-be.js",
            "fr-CA": "node_modules/sp-client-custom-fields/lib/loc/fr-ca.js",
            "fr-CH": "node_modules/sp-client-custom-fields/lib/loc/fr-ch.js",
            "fr-FR": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "gsw": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "br": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "tzm-Tfng": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "co": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "fr": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "ff": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "lb": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "mg": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "oc": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "zgh": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "wo": "node_modules/sp-client-custom-fields/lib/loc/fr-fr.js",
            "hi-IN": "node_modules/sp-client-custom-fields/lib/loc/hi-in.js",
            "hi": "node_modules/sp-client-custom-fields/lib/loc/hi-in.js",
            "hu-HU": "node_modules/sp-client-custom-fields/lib/loc/hu-hu.js",
            "it-CH": "node_modules/sp-client-custom-fields/lib/loc/it-ch.js",
            "it-IT": "node_modules/sp-client-custom-fields/lib/loc/it-it.js",
            "it": "node_modules/sp-client-custom-fields/lib/loc/it-it.js",
            "ja-JP": "node_modules/sp-client-custom-fields/lib/loc/ja-jp.js",
            "nb-NO": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "no": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "nb": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "nn": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "smj-NO": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "smj-Latn-NO": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "se-NO": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "se-Latn-NO": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "sma-Latn": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "sma-NO": "node_modules/sp-client-custom-fields/lib/loc/nb-no.js",
            "nl-NL": "node_modules/sp-client-custom-fields/lib/loc/nl-nl.js",
            "nl": "node_modules/sp-client-custom-fields/lib/loc/nl-nl.js",
            "fy": "node_modules/sp-client-custom-fields/lib/loc/nl-nl.js",
            "nn-NO": "node_modules/sp-client-custom-fields/lib/loc/nn-no.js",
            "pt-BR": "node_modules/sp-client-custom-fields/lib/loc/pt-br.js",
            "pt-PT": "node_modules/sp-client-custom-fields/lib/loc/pt-pt.js",
            "pt": "node_modules/sp-client-custom-fields/lib/loc/pt-pt.js",
            "ru-RU": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "ru": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "ba": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "be": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "ky": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "mn": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "sah": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "tg": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "tt": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "tk": "node_modules/sp-client-custom-fields/lib/loc/ru-ru.js",
            "sv-SE": "node_modules/sp-client-custom-fields/lib/loc/sv-se.js",
            "smj": "node_modules/sp-client-custom-fields/lib/loc/sv-se.js",
            "se": "node_modules/sp-client-custom-fields/lib/loc/sv-se.js",
            "sv": "node_modules/sp-client-custom-fields/lib/loc/sv-se.js",
            "sma-SE": "node_modules/sp-client-custom-fields/lib/loc/sv-se.js",
            "sma-Latn-SE": "node_modules/sp-client-custom-fields/lib/loc/sv-se.js",
            "th-TH": "node_modules/sp-client-custom-fields/lib/loc/th-th.js",
            "th": "node_modules/sp-client-custom-fields/lib/loc/th-th.js",
            "tr-TR": "node_modules/sp-client-custom-fields/lib/loc/tr-tr.js",
            "tr": "node_modules/sp-client-custom-fields/lib/loc/tr-tr.js",
            "zh-CN": "node_modules/sp-client-custom-fields/lib/loc/zh-cn.js",
            "zh": "node_modules/sp-client-custom-fields/lib/loc/zh-cn.js",
            "mn-Mong": "node_modules/sp-client-custom-fields/lib/loc/zh-cn.js",
            "bo": "node_modules/sp-client-custom-fields/lib/loc/zh-cn.js",
            "ug": "node_modules/sp-client-custom-fields/lib/loc/zh-cn.js",
            "ii": "node_modules/sp-client-custom-fields/lib/loc/zh-cn.js"
          }
        },
        "PropertyControlStrings": {
          "defaultPath": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
          "type": "localizedPath",
          "paths": {
            "en-US": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "bn": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "chr": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "dv": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "div": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "en": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "fil": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "haw": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "iu": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "lo": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "moh": "node_modules/@pnp/spfx-property-controls/lib/loc/en-us.js",
            "fr-FR": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "gsw": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "br": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "tzm-Tfng": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "co": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "fr": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "ff": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "lb": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "mg": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "oc": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "zgh": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "wo": "node_modules/@pnp/spfx-property-controls/lib/loc/fr-fr.js",
            "nl-NL": "node_modules/@pnp/spfx-property-controls/lib/loc/nl-nl.js",
            "nl": "node_modules/@pnp/spfx-property-controls/lib/loc/nl-nl.js",
            "fy": "node_modules/@pnp/spfx-property-controls/lib/loc/nl-nl.js",
            "ru-RU": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "ru": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "ba": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "be": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "ky": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "mn": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "sah": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "tg": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "tt": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "tk": "node_modules/@pnp/spfx-property-controls/lib/loc/ru-ru.js",
            "zh-CN": "node_modules/@pnp/spfx-property-controls/lib/loc/zh-cn.js",
            "zh": "node_modules/@pnp/spfx-property-controls/lib/loc/zh-cn.js",
            "mn-Mong": "node_modules/@pnp/spfx-property-controls/lib/loc/zh-cn.js",
            "bo": "node_modules/@pnp/spfx-property-controls/lib/loc/zh-cn.js",
            "ug": "node_modules/@pnp/spfx-property-controls/lib/loc/zh-cn.js",
            "ii": "node_modules/@pnp/spfx-property-controls/lib/loc/zh-cn.js"
          }
        },
        "FphJobVacancyWebPartStrings": {
          "defaultPath": "lib/webparts/fphJobVacancy/loc/en-us.js",
          "type": "localizedPath",
          "paths": {}
        },
        "react": {
          "type": "component",
          "version": "16.7.0",
          "id": "0d910c1c-13b9-4e1c-9aa4-b008c5e42d7d",
          "failoverPath": "node_modules/react/dist/react.js"
        },
        "react-dom": {
          "type": "component",
          "version": "16.7.0",
          "id": "aa0a46ec-1505-43cd-a44a-93f3a5aa460a",
          "failoverPath": "node_modules/react-dom/dist/react-dom.js"
        },
        "@microsoft/sp-webpart-base": {
          "type": "component",
          "version": "1.8.2",
          "id": "974a7777-0990-4136-8fa6-95d80114c2e0"
        },
        "@microsoft/sp-core-library": {
          "type": "component",
          "version": "1.1.0",
          "id": "7263c7d0-1d6a-45ec-8d85-d4d1d234171b"
        },
        "@microsoft/sp-property-pane": {
          "type": "component",
          "version": "1.8.2",
          "id": "f9e737b7-f0df-4597-ba8c-3060f82380db"
        }
      }
    }
  }
];
}
exports.getManifests = getManifests;
//# sourceMappingURL=manifestsFile.js.map

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var punycode = __webpack_require__(2);
var util = __webpack_require__(5);

exports.parse = urlParse;
exports.resolve = urlResolve;
exports.resolveObject = urlResolveObject;
exports.format = urlFormat;

exports.Url = Url;

function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.host = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.query = null;
  this.pathname = null;
  this.path = null;
  this.href = null;
}

// Reference: RFC 3986, RFC 1808, RFC 2396

// define these here so at least they only have to be
// compiled once on the first module load.
var protocolPattern = /^([a-z0-9.+-]+:)/i,
    portPattern = /:[0-9]*$/,

    // Special case for a simple path URL
    simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,

    // RFC 2396: characters reserved for delimiting URLs.
    // We actually just auto-escape these.
    delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'],

    // RFC 2396: characters not allowed for various reasons.
    unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims),

    // Allowed by RFCs, but cause of XSS attacks.  Always escape these.
    autoEscape = ['\''].concat(unwise),
    // Characters that are never ever allowed in a hostname.
    // Note that any invalid chars are also handled, but these
    // are the ones that are *expected* to be seen, so we fast-path
    // them.
    nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape),
    hostEndingChars = ['/', '?', '#'],
    hostnameMaxLen = 255,
    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
    // protocols that can allow "unsafe" and "unwise" chars.
    unsafeProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that never have a hostname.
    hostlessProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that always contain a // bit.
    slashedProtocol = {
      'http': true,
      'https': true,
      'ftp': true,
      'gopher': true,
      'file': true,
      'http:': true,
      'https:': true,
      'ftp:': true,
      'gopher:': true,
      'file:': true
    },
    querystring = __webpack_require__(6);

function urlParse(url, parseQueryString, slashesDenoteHost) {
  if (url && util.isObject(url) && url instanceof Url) return url;

  var u = new Url;
  u.parse(url, parseQueryString, slashesDenoteHost);
  return u;
}

Url.prototype.parse = function(url, parseQueryString, slashesDenoteHost) {
  if (!util.isString(url)) {
    throw new TypeError("Parameter 'url' must be a string, not " + typeof url);
  }

  // Copy chrome, IE, opera backslash-handling behavior.
  // Back slashes before the query string get converted to forward slashes
  // See: https://code.google.com/p/chromium/issues/detail?id=25916
  var queryIndex = url.indexOf('?'),
      splitter =
          (queryIndex !== -1 && queryIndex < url.indexOf('#')) ? '?' : '#',
      uSplit = url.split(splitter),
      slashRegex = /\\/g;
  uSplit[0] = uSplit[0].replace(slashRegex, '/');
  url = uSplit.join(splitter);

  var rest = url;

  // trim before proceeding.
  // This is to support parse stuff like "  http://foo.com  \n"
  rest = rest.trim();

  if (!slashesDenoteHost && url.split('#').length === 1) {
    // Try fast path regexp
    var simplePath = simplePathPattern.exec(rest);
    if (simplePath) {
      this.path = rest;
      this.href = rest;
      this.pathname = simplePath[1];
      if (simplePath[2]) {
        this.search = simplePath[2];
        if (parseQueryString) {
          this.query = querystring.parse(this.search.substr(1));
        } else {
          this.query = this.search.substr(1);
        }
      } else if (parseQueryString) {
        this.search = '';
        this.query = {};
      }
      return this;
    }
  }

  var proto = protocolPattern.exec(rest);
  if (proto) {
    proto = proto[0];
    var lowerProto = proto.toLowerCase();
    this.protocol = lowerProto;
    rest = rest.substr(proto.length);
  }

  // figure out if it's got a host
  // user@server is *always* interpreted as a hostname, and url
  // resolution will treat //foo/bar as host=foo,path=bar because that's
  // how the browser resolves relative URLs.
  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    var slashes = rest.substr(0, 2) === '//';
    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }

  if (!hostlessProtocol[proto] &&
      (slashes || (proto && !slashedProtocol[proto]))) {

    // there's a hostname.
    // the first instance of /, ?, ;, or # ends the host.
    //
    // If there is an @ in the hostname, then non-host chars *are* allowed
    // to the left of the last @ sign, unless some host-ending character
    // comes *before* the @-sign.
    // URLs are obnoxious.
    //
    // ex:
    // http://a@b@c/ => user:a@b host:c
    // http://a@b?@c => user:a host:c path:/?@c

    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
    // Review our test case against browsers more comprehensively.

    // find the first instance of any hostEndingChars
    var hostEnd = -1;
    for (var i = 0; i < hostEndingChars.length; i++) {
      var hec = rest.indexOf(hostEndingChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }

    // at this point, either we have an explicit point where the
    // auth portion cannot go past, or the last @ char is the decider.
    var auth, atSign;
    if (hostEnd === -1) {
      // atSign can be anywhere.
      atSign = rest.lastIndexOf('@');
    } else {
      // atSign must be in auth portion.
      // http://a@b/c@d => host:b auth:a path:/c@d
      atSign = rest.lastIndexOf('@', hostEnd);
    }

    // Now we have a portion which is definitely the auth.
    // Pull that off.
    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = decodeURIComponent(auth);
    }

    // the host is the remaining to the left of the first non-host char
    hostEnd = -1;
    for (var i = 0; i < nonHostChars.length; i++) {
      var hec = rest.indexOf(nonHostChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }
    // if we still have not hit it, then the entire thing is a host.
    if (hostEnd === -1)
      hostEnd = rest.length;

    this.host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd);

    // pull out port.
    this.parseHost();

    // we've indicated that there is a hostname,
    // so even if it's empty, it has to be present.
    this.hostname = this.hostname || '';

    // if hostname begins with [ and ends with ]
    // assume that it's an IPv6 address.
    var ipv6Hostname = this.hostname[0] === '[' &&
        this.hostname[this.hostname.length - 1] === ']';

    // validate a little.
    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);
      for (var i = 0, l = hostparts.length; i < l; i++) {
        var part = hostparts[i];
        if (!part) continue;
        if (!part.match(hostnamePartPattern)) {
          var newpart = '';
          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              // we replace non-ASCII char with a temporary placeholder
              // we need this to make sure size of hostname is not
              // broken by replacing non-ASCII by nothing
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          }
          // we test again with ASCII char only
          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);
            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }
            if (notHost.length) {
              rest = '/' + notHost.join('.') + rest;
            }
            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }

    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    } else {
      // hostnames are always lower case.
      this.hostname = this.hostname.toLowerCase();
    }

    if (!ipv6Hostname) {
      // IDNA Support: Returns a punycoded representation of "domain".
      // It only converts parts of the domain name that
      // have non-ASCII characters, i.e. it doesn't matter if
      // you call it with a domain that already is ASCII-only.
      this.hostname = punycode.toASCII(this.hostname);
    }

    var p = this.port ? ':' + this.port : '';
    var h = this.hostname || '';
    this.host = h + p;
    this.href += this.host;

    // strip [ and ] from the hostname
    // the host field still retains them, though
    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
      if (rest[0] !== '/') {
        rest = '/' + rest;
      }
    }
  }

  // now rest is set to the post-host stuff.
  // chop off any delim chars.
  if (!unsafeProtocol[lowerProto]) {

    // First, make 100% sure that any "autoEscape" chars get
    // escaped, even if encodeURIComponent doesn't think they
    // need to be.
    for (var i = 0, l = autoEscape.length; i < l; i++) {
      var ae = autoEscape[i];
      if (rest.indexOf(ae) === -1)
        continue;
      var esc = encodeURIComponent(ae);
      if (esc === ae) {
        esc = escape(ae);
      }
      rest = rest.split(ae).join(esc);
    }
  }


  // chop off from the tail first.
  var hash = rest.indexOf('#');
  if (hash !== -1) {
    // got a fragment string.
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }
  var qm = rest.indexOf('?');
  if (qm !== -1) {
    this.search = rest.substr(qm);
    this.query = rest.substr(qm + 1);
    if (parseQueryString) {
      this.query = querystring.parse(this.query);
    }
    rest = rest.slice(0, qm);
  } else if (parseQueryString) {
    // no query string, but parseQueryString still requested
    this.search = '';
    this.query = {};
  }
  if (rest) this.pathname = rest;
  if (slashedProtocol[lowerProto] &&
      this.hostname && !this.pathname) {
    this.pathname = '/';
  }

  //to support http.request
  if (this.pathname || this.search) {
    var p = this.pathname || '';
    var s = this.search || '';
    this.path = p + s;
  }

  // finally, reconstruct the href based on what has been validated.
  this.href = this.format();
  return this;
};

// format a parsed object into a url string
function urlFormat(obj) {
  // ensure it's an object, and not a string url.
  // If it's an obj, this is a no-op.
  // this way, you can call url_format() on strings
  // to clean up potentially wonky urls.
  if (util.isString(obj)) obj = urlParse(obj);
  if (!(obj instanceof Url)) return Url.prototype.format.call(obj);
  return obj.format();
}

Url.prototype.format = function() {
  var auth = this.auth || '';
  if (auth) {
    auth = encodeURIComponent(auth);
    auth = auth.replace(/%3A/i, ':');
    auth += '@';
  }

  var protocol = this.protocol || '',
      pathname = this.pathname || '',
      hash = this.hash || '',
      host = false,
      query = '';

  if (this.host) {
    host = auth + this.host;
  } else if (this.hostname) {
    host = auth + (this.hostname.indexOf(':') === -1 ?
        this.hostname :
        '[' + this.hostname + ']');
    if (this.port) {
      host += ':' + this.port;
    }
  }

  if (this.query &&
      util.isObject(this.query) &&
      Object.keys(this.query).length) {
    query = querystring.stringify(this.query);
  }

  var search = this.search || (query && ('?' + query)) || '';

  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

  // only the slashedProtocols get the //.  Not mailto:, xmpp:, etc.
  // unless they had them to begin with.
  if (this.slashes ||
      (!protocol || slashedProtocol[protocol]) && host !== false) {
    host = '//' + (host || '');
    if (pathname && pathname.charAt(0) !== '/') pathname = '/' + pathname;
  } else if (!host) {
    host = '';
  }

  if (hash && hash.charAt(0) !== '#') hash = '#' + hash;
  if (search && search.charAt(0) !== '?') search = '?' + search;

  pathname = pathname.replace(/[?#]/g, function(match) {
    return encodeURIComponent(match);
  });
  search = search.replace('#', '%23');

  return protocol + host + pathname + search + hash;
};

function urlResolve(source, relative) {
  return urlParse(source, false, true).resolve(relative);
}

Url.prototype.resolve = function(relative) {
  return this.resolveObject(urlParse(relative, false, true)).format();
};

function urlResolveObject(source, relative) {
  if (!source) return relative;
  return urlParse(source, false, true).resolveObject(relative);
}

Url.prototype.resolveObject = function(relative) {
  if (util.isString(relative)) {
    var rel = new Url();
    rel.parse(relative, false, true);
    relative = rel;
  }

  var result = new Url();
  var tkeys = Object.keys(this);
  for (var tk = 0; tk < tkeys.length; tk++) {
    var tkey = tkeys[tk];
    result[tkey] = this[tkey];
  }

  // hash is always overridden, no matter what.
  // even href="" will remove it.
  result.hash = relative.hash;

  // if the relative url is empty, then there's nothing left to do here.
  if (relative.href === '') {
    result.href = result.format();
    return result;
  }

  // hrefs like //foo/bar always cut to the protocol.
  if (relative.slashes && !relative.protocol) {
    // take everything except the protocol from relative
    var rkeys = Object.keys(relative);
    for (var rk = 0; rk < rkeys.length; rk++) {
      var rkey = rkeys[rk];
      if (rkey !== 'protocol')
        result[rkey] = relative[rkey];
    }

    //urlParse appends trailing / to urls like http://www.example.com
    if (slashedProtocol[result.protocol] &&
        result.hostname && !result.pathname) {
      result.path = result.pathname = '/';
    }

    result.href = result.format();
    return result;
  }

  if (relative.protocol && relative.protocol !== result.protocol) {
    // if it's a known url protocol, then changing
    // the protocol does weird things
    // first, if it's not file:, then we MUST have a host,
    // and if there was a path
    // to begin with, then we MUST have a path.
    // if it is file:, then the host is dropped,
    // because that's known to be hostless.
    // anything else is assumed to be absolute.
    if (!slashedProtocol[relative.protocol]) {
      var keys = Object.keys(relative);
      for (var v = 0; v < keys.length; v++) {
        var k = keys[v];
        result[k] = relative[k];
      }
      result.href = result.format();
      return result;
    }

    result.protocol = relative.protocol;
    if (!relative.host && !hostlessProtocol[relative.protocol]) {
      var relPath = (relative.pathname || '').split('/');
      while (relPath.length && !(relative.host = relPath.shift()));
      if (!relative.host) relative.host = '';
      if (!relative.hostname) relative.hostname = '';
      if (relPath[0] !== '') relPath.unshift('');
      if (relPath.length < 2) relPath.unshift('');
      result.pathname = relPath.join('/');
    } else {
      result.pathname = relative.pathname;
    }
    result.search = relative.search;
    result.query = relative.query;
    result.host = relative.host || '';
    result.auth = relative.auth;
    result.hostname = relative.hostname || relative.host;
    result.port = relative.port;
    // to support http.request
    if (result.pathname || result.search) {
      var p = result.pathname || '';
      var s = result.search || '';
      result.path = p + s;
    }
    result.slashes = result.slashes || relative.slashes;
    result.href = result.format();
    return result;
  }

  var isSourceAbs = (result.pathname && result.pathname.charAt(0) === '/'),
      isRelAbs = (
          relative.host ||
          relative.pathname && relative.pathname.charAt(0) === '/'
      ),
      mustEndAbs = (isRelAbs || isSourceAbs ||
                    (result.host && relative.pathname)),
      removeAllDots = mustEndAbs,
      srcPath = result.pathname && result.pathname.split('/') || [],
      relPath = relative.pathname && relative.pathname.split('/') || [],
      psychotic = result.protocol && !slashedProtocol[result.protocol];

  // if the url is a non-slashed url, then relative
  // links like ../.. should be able
  // to crawl up to the hostname, as well.  This is strange.
  // result.protocol has already been set by now.
  // Later on, put the first path part into the host field.
  if (psychotic) {
    result.hostname = '';
    result.port = null;
    if (result.host) {
      if (srcPath[0] === '') srcPath[0] = result.host;
      else srcPath.unshift(result.host);
    }
    result.host = '';
    if (relative.protocol) {
      relative.hostname = null;
      relative.port = null;
      if (relative.host) {
        if (relPath[0] === '') relPath[0] = relative.host;
        else relPath.unshift(relative.host);
      }
      relative.host = null;
    }
    mustEndAbs = mustEndAbs && (relPath[0] === '' || srcPath[0] === '');
  }

  if (isRelAbs) {
    // it's absolute.
    result.host = (relative.host || relative.host === '') ?
                  relative.host : result.host;
    result.hostname = (relative.hostname || relative.hostname === '') ?
                      relative.hostname : result.hostname;
    result.search = relative.search;
    result.query = relative.query;
    srcPath = relPath;
    // fall through to the dot-handling below.
  } else if (relPath.length) {
    // it's relative
    // throw away the existing file, and take the new path instead.
    if (!srcPath) srcPath = [];
    srcPath.pop();
    srcPath = srcPath.concat(relPath);
    result.search = relative.search;
    result.query = relative.query;
  } else if (!util.isNullOrUndefined(relative.search)) {
    // just pull out the search.
    // like href='?foo'.
    // Put this after the other two cases because it simplifies the booleans
    if (psychotic) {
      result.hostname = result.host = srcPath.shift();
      //occationaly the auth can get stuck only in host
      //this especially happens in cases like
      //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
      var authInHost = result.host && result.host.indexOf('@') > 0 ?
                       result.host.split('@') : false;
      if (authInHost) {
        result.auth = authInHost.shift();
        result.host = result.hostname = authInHost.shift();
      }
    }
    result.search = relative.search;
    result.query = relative.query;
    //to support http.request
    if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
      result.path = (result.pathname ? result.pathname : '') +
                    (result.search ? result.search : '');
    }
    result.href = result.format();
    return result;
  }

  if (!srcPath.length) {
    // no path at all.  easy.
    // we've already handled the other stuff above.
    result.pathname = null;
    //to support http.request
    if (result.search) {
      result.path = '/' + result.search;
    } else {
      result.path = null;
    }
    result.href = result.format();
    return result;
  }

  // if a url ENDs in . or .., then it must get a trailing slash.
  // however, if it ends in anything else non-slashy,
  // then it must NOT get a trailing slash.
  var last = srcPath.slice(-1)[0];
  var hasTrailingSlash = (
      (result.host || relative.host || srcPath.length > 1) &&
      (last === '.' || last === '..') || last === '');

  // strip single dots, resolve double dots to parent dir
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = srcPath.length; i >= 0; i--) {
    last = srcPath[i];
    if (last === '.') {
      srcPath.splice(i, 1);
    } else if (last === '..') {
      srcPath.splice(i, 1);
      up++;
    } else if (up) {
      srcPath.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (!mustEndAbs && !removeAllDots) {
    for (; up--; up) {
      srcPath.unshift('..');
    }
  }

  if (mustEndAbs && srcPath[0] !== '' &&
      (!srcPath[0] || srcPath[0].charAt(0) !== '/')) {
    srcPath.unshift('');
  }

  if (hasTrailingSlash && (srcPath.join('/').substr(-1) !== '/')) {
    srcPath.push('');
  }

  var isAbsolute = srcPath[0] === '' ||
      (srcPath[0] && srcPath[0].charAt(0) === '/');

  // put the host back
  if (psychotic) {
    result.hostname = result.host = isAbsolute ? '' :
                                    srcPath.length ? srcPath.shift() : '';
    //occationaly the auth can get stuck only in host
    //this especially happens in cases like
    //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
    var authInHost = result.host && result.host.indexOf('@') > 0 ?
                     result.host.split('@') : false;
    if (authInHost) {
      result.auth = authInHost.shift();
      result.host = result.hostname = authInHost.shift();
    }
  }

  mustEndAbs = mustEndAbs || (result.host && srcPath.length);

  if (mustEndAbs && !isAbsolute) {
    srcPath.unshift('');
  }

  if (!srcPath.length) {
    result.pathname = null;
    result.path = null;
  } else {
    result.pathname = srcPath.join('/');
  }

  //to support request.http
  if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
    result.path = (result.pathname ? result.pathname : '') +
                  (result.search ? result.search : '');
  }
  result.auth = relative.auth || result.auth;
  result.slashes = result.slashes || relative.slashes;
  result.href = result.format();
  return result;
};

Url.prototype.parseHost = function() {
  var host = this.host;
  var port = portPattern.exec(host);
  if (port) {
    port = port[0];
    if (port !== ':') {
      this.port = port.substr(1);
    }
    host = host.substr(0, host.length - port.length);
  }
  if (host) this.hostname = host;
};


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, global) {var __WEBPACK_AMD_DEFINE_RESULT__;/*! https://mths.be/punycode v1.4.1 by @mathias */
;(function(root) {

	/** Detect free variables */
	var freeExports = typeof exports == 'object' && exports &&
		!exports.nodeType && exports;
	var freeModule = typeof module == 'object' && module &&
		!module.nodeType && module;
	var freeGlobal = typeof global == 'object' && global;
	if (
		freeGlobal.global === freeGlobal ||
		freeGlobal.window === freeGlobal ||
		freeGlobal.self === freeGlobal
	) {
		root = freeGlobal;
	}

	/**
	 * The `punycode` object.
	 * @name punycode
	 * @type Object
	 */
	var punycode,

	/** Highest positive signed 32-bit float value */
	maxInt = 2147483647, // aka. 0x7FFFFFFF or 2^31-1

	/** Bootstring parameters */
	base = 36,
	tMin = 1,
	tMax = 26,
	skew = 38,
	damp = 700,
	initialBias = 72,
	initialN = 128, // 0x80
	delimiter = '-', // '\x2D'

	/** Regular expressions */
	regexPunycode = /^xn--/,
	regexNonASCII = /[^\x20-\x7E]/, // unprintable ASCII chars + non-ASCII chars
	regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g, // RFC 3490 separators

	/** Error messages */
	errors = {
		'overflow': 'Overflow: input needs wider integers to process',
		'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
		'invalid-input': 'Invalid input'
	},

	/** Convenience shortcuts */
	baseMinusTMin = base - tMin,
	floor = Math.floor,
	stringFromCharCode = String.fromCharCode,

	/** Temporary variable */
	key;

	/*--------------------------------------------------------------------------*/

	/**
	 * A generic error utility function.
	 * @private
	 * @param {String} type The error type.
	 * @returns {Error} Throws a `RangeError` with the applicable error message.
	 */
	function error(type) {
		throw new RangeError(errors[type]);
	}

	/**
	 * A generic `Array#map` utility function.
	 * @private
	 * @param {Array} array The array to iterate over.
	 * @param {Function} callback The function that gets called for every array
	 * item.
	 * @returns {Array} A new array of values returned by the callback function.
	 */
	function map(array, fn) {
		var length = array.length;
		var result = [];
		while (length--) {
			result[length] = fn(array[length]);
		}
		return result;
	}

	/**
	 * A simple `Array#map`-like wrapper to work with domain name strings or email
	 * addresses.
	 * @private
	 * @param {String} domain The domain name or email address.
	 * @param {Function} callback The function that gets called for every
	 * character.
	 * @returns {Array} A new string of characters returned by the callback
	 * function.
	 */
	function mapDomain(string, fn) {
		var parts = string.split('@');
		var result = '';
		if (parts.length > 1) {
			// In email addresses, only the domain name should be punycoded. Leave
			// the local part (i.e. everything up to `@`) intact.
			result = parts[0] + '@';
			string = parts[1];
		}
		// Avoid `split(regex)` for IE8 compatibility. See #17.
		string = string.replace(regexSeparators, '\x2E');
		var labels = string.split('.');
		var encoded = map(labels, fn).join('.');
		return result + encoded;
	}

	/**
	 * Creates an array containing the numeric code points of each Unicode
	 * character in the string. While JavaScript uses UCS-2 internally,
	 * this function will convert a pair of surrogate halves (each of which
	 * UCS-2 exposes as separate characters) into a single code point,
	 * matching UTF-16.
	 * @see `punycode.ucs2.encode`
	 * @see <https://mathiasbynens.be/notes/javascript-encoding>
	 * @memberOf punycode.ucs2
	 * @name decode
	 * @param {String} string The Unicode input string (UCS-2).
	 * @returns {Array} The new array of code points.
	 */
	function ucs2decode(string) {
		var output = [],
		    counter = 0,
		    length = string.length,
		    value,
		    extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
					output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// unmatched surrogate; only append this code unit, in case the next
					// code unit is the high surrogate of a surrogate pair
					output.push(value);
					counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	/**
	 * Creates a string based on an array of numeric code points.
	 * @see `punycode.ucs2.decode`
	 * @memberOf punycode.ucs2
	 * @name encode
	 * @param {Array} codePoints The array of numeric code points.
	 * @returns {String} The new Unicode string (UCS-2).
	 */
	function ucs2encode(array) {
		return map(array, function(value) {
			var output = '';
			if (value > 0xFFFF) {
				value -= 0x10000;
				output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
				value = 0xDC00 | value & 0x3FF;
			}
			output += stringFromCharCode(value);
			return output;
		}).join('');
	}

	/**
	 * Converts a basic code point into a digit/integer.
	 * @see `digitToBasic()`
	 * @private
	 * @param {Number} codePoint The basic numeric code point value.
	 * @returns {Number} The numeric value of a basic code point (for use in
	 * representing integers) in the range `0` to `base - 1`, or `base` if
	 * the code point does not represent a value.
	 */
	function basicToDigit(codePoint) {
		if (codePoint - 48 < 10) {
			return codePoint - 22;
		}
		if (codePoint - 65 < 26) {
			return codePoint - 65;
		}
		if (codePoint - 97 < 26) {
			return codePoint - 97;
		}
		return base;
	}

	/**
	 * Converts a digit/integer into a basic code point.
	 * @see `basicToDigit()`
	 * @private
	 * @param {Number} digit The numeric value of a basic code point.
	 * @returns {Number} The basic code point whose value (when used for
	 * representing integers) is `digit`, which needs to be in the range
	 * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
	 * used; else, the lowercase form is used. The behavior is undefined
	 * if `flag` is non-zero and `digit` has no uppercase form.
	 */
	function digitToBasic(digit, flag) {
		//  0..25 map to ASCII a..z or A..Z
		// 26..35 map to ASCII 0..9
		return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
	}

	/**
	 * Bias adaptation function as per section 3.4 of RFC 3492.
	 * https://tools.ietf.org/html/rfc3492#section-3.4
	 * @private
	 */
	function adapt(delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime ? floor(delta / damp) : delta >> 1;
		delta += floor(delta / numPoints);
		for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
			delta = floor(delta / baseMinusTMin);
		}
		return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
	}

	/**
	 * Converts a Punycode string of ASCII-only symbols to a string of Unicode
	 * symbols.
	 * @memberOf punycode
	 * @param {String} input The Punycode string of ASCII-only symbols.
	 * @returns {String} The resulting string of Unicode symbols.
	 */
	function decode(input) {
		// Don't use UCS-2
		var output = [],
		    inputLength = input.length,
		    out,
		    i = 0,
		    n = initialN,
		    bias = initialBias,
		    basic,
		    j,
		    index,
		    oldi,
		    w,
		    k,
		    digit,
		    t,
		    /** Cached calculation results */
		    baseMinusT;

		// Handle the basic code points: let `basic` be the number of input code
		// points before the last delimiter, or `0` if there is none, then copy
		// the first basic code points to the output.

		basic = input.lastIndexOf(delimiter);
		if (basic < 0) {
			basic = 0;
		}

		for (j = 0; j < basic; ++j) {
			// if it's not a basic code point
			if (input.charCodeAt(j) >= 0x80) {
				error('not-basic');
			}
			output.push(input.charCodeAt(j));
		}

		// Main decoding loop: start just after the last delimiter if any basic code
		// points were copied; start at the beginning otherwise.

		for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {

			// `index` is the index of the next character to be consumed.
			// Decode a generalized variable-length integer into `delta`,
			// which gets added to `i`. The overflow checking is easier
			// if we increase `i` as we go, then subtract off its starting
			// value at the end to obtain `delta`.
			for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

				if (index >= inputLength) {
					error('invalid-input');
				}

				digit = basicToDigit(input.charCodeAt(index++));

				if (digit >= base || digit > floor((maxInt - i) / w)) {
					error('overflow');
				}

				i += digit * w;
				t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

				if (digit < t) {
					break;
				}

				baseMinusT = base - t;
				if (w > floor(maxInt / baseMinusT)) {
					error('overflow');
				}

				w *= baseMinusT;

			}

			out = output.length + 1;
			bias = adapt(i - oldi, out, oldi == 0);

			// `i` was supposed to wrap around from `out` to `0`,
			// incrementing `n` each time, so we'll fix that now:
			if (floor(i / out) > maxInt - n) {
				error('overflow');
			}

			n += floor(i / out);
			i %= out;

			// Insert `n` at position `i` of the output
			output.splice(i++, 0, n);

		}

		return ucs2encode(output);
	}

	/**
	 * Converts a string of Unicode symbols (e.g. a domain name label) to a
	 * Punycode string of ASCII-only symbols.
	 * @memberOf punycode
	 * @param {String} input The string of Unicode symbols.
	 * @returns {String} The resulting Punycode string of ASCII-only symbols.
	 */
	function encode(input) {
		var n,
		    delta,
		    handledCPCount,
		    basicLength,
		    bias,
		    j,
		    m,
		    q,
		    k,
		    t,
		    currentValue,
		    output = [],
		    /** `inputLength` will hold the number of code points in `input`. */
		    inputLength,
		    /** Cached calculation results */
		    handledCPCountPlusOne,
		    baseMinusT,
		    qMinusT;

		// Convert the input in UCS-2 to Unicode
		input = ucs2decode(input);

		// Cache the length
		inputLength = input.length;

		// Initialize the state
		n = initialN;
		delta = 0;
		bias = initialBias;

		// Handle the basic code points
		for (j = 0; j < inputLength; ++j) {
			currentValue = input[j];
			if (currentValue < 0x80) {
				output.push(stringFromCharCode(currentValue));
			}
		}

		handledCPCount = basicLength = output.length;

		// `handledCPCount` is the number of code points that have been handled;
		// `basicLength` is the number of basic code points.

		// Finish the basic string - if it is not empty - with a delimiter
		if (basicLength) {
			output.push(delimiter);
		}

		// Main encoding loop:
		while (handledCPCount < inputLength) {

			// All non-basic code points < n have been handled already. Find the next
			// larger one:
			for (m = maxInt, j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue >= n && currentValue < m) {
					m = currentValue;
				}
			}

			// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
			// but guard against overflow
			handledCPCountPlusOne = handledCPCount + 1;
			if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
				error('overflow');
			}

			delta += (m - n) * handledCPCountPlusOne;
			n = m;

			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];

				if (currentValue < n && ++delta > maxInt) {
					error('overflow');
				}

				if (currentValue == n) {
					// Represent delta as a generalized variable-length integer
					for (q = delta, k = base; /* no condition */; k += base) {
						t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
						if (q < t) {
							break;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						output.push(
							stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
						);
						q = floor(qMinusT / baseMinusT);
					}

					output.push(stringFromCharCode(digitToBasic(q, 0)));
					bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
					delta = 0;
					++handledCPCount;
				}
			}

			++delta;
			++n;

		}
		return output.join('');
	}

	/**
	 * Converts a Punycode string representing a domain name or an email address
	 * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
	 * it doesn't matter if you call it on a string that has already been
	 * converted to Unicode.
	 * @memberOf punycode
	 * @param {String} input The Punycoded domain name or email address to
	 * convert to Unicode.
	 * @returns {String} The Unicode representation of the given Punycode
	 * string.
	 */
	function toUnicode(input) {
		return mapDomain(input, function(string) {
			return regexPunycode.test(string)
				? decode(string.slice(4).toLowerCase())
				: string;
		});
	}

	/**
	 * Converts a Unicode string representing a domain name or an email address to
	 * Punycode. Only the non-ASCII parts of the domain name will be converted,
	 * i.e. it doesn't matter if you call it with a domain that's already in
	 * ASCII.
	 * @memberOf punycode
	 * @param {String} input The domain name or email address to convert, as a
	 * Unicode string.
	 * @returns {String} The Punycode representation of the given domain name or
	 * email address.
	 */
	function toASCII(input) {
		return mapDomain(input, function(string) {
			return regexNonASCII.test(string)
				? 'xn--' + encode(string)
				: string;
		});
	}

	/*--------------------------------------------------------------------------*/

	/** Define the public API */
	punycode = {
		/**
		 * A string representing the current Punycode.js version number.
		 * @memberOf punycode
		 * @type String
		 */
		'version': '1.4.1',
		/**
		 * An object of methods to convert from JavaScript's internal character
		 * representation (UCS-2) to Unicode code points, and back.
		 * @see <https://mathiasbynens.be/notes/javascript-encoding>
		 * @memberOf punycode
		 * @type Object
		 */
		'ucs2': {
			'decode': ucs2decode,
			'encode': ucs2encode
		},
		'decode': decode,
		'encode': encode,
		'toASCII': toASCII,
		'toUnicode': toUnicode
	};

	/** Expose `punycode` */
	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		true
	) {
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
			return punycode;
		}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else if (freeExports && freeModule) {
		if (module.exports == freeExports) {
			// in Node.js, io.js, or RingoJS v0.8.0+
			freeModule.exports = punycode;
		} else {
			// in Narwhal or RingoJS v0.7.0-
			for (key in punycode) {
				punycode.hasOwnProperty(key) && (freeExports[key] = punycode[key]);
			}
		}
	} else {
		// in Rhino or a web browser
		root.punycode = punycode;
	}

}(this));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)(module), __webpack_require__(4)))

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 4 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
  isString: function(arg) {
    return typeof(arg) === 'string';
  },
  isObject: function(arg) {
    return typeof(arg) === 'object' && arg !== null;
  },
  isNull: function(arg) {
    return arg === null;
  },
  isNullOrUndefined: function(arg) {
    return arg == null;
  }
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.decode = exports.parse = __webpack_require__(7);
exports.encode = exports.stringify = __webpack_require__(8);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



// If obj.hasOwnProperty has been overridden, then calling
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707
function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

module.exports = function(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);

  var maxKeys = 1000;
  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length;
  // maxKeys <= 0 means that we should not limit keys count
  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr, vstr, k, v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var stringifyPrimitive = function(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

module.exports = function(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';
  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return map(objectKeys(obj), function(k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
      if (isArray(obj[k])) {
        return map(obj[k], function(v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);

  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq +
         encodeURIComponent(stringifyPrimitive(obj));
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

function map (xs, f) {
  if (xs.map) return xs.map(f);
  var res = [];
  for (var i = 0; i < xs.length; i++) {
    res.push(f(xs[i], i));
  }
  return res;
}

var objectKeys = Object.keys || function (obj) {
  var res = [];
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
  }
  return res;
};


/***/ })
/******/ ]);
});