declare const styles: {
    'job-entry': string;
    '-webkit-mask': string;
    mask: string;
    wrapper: string;
    'company-icon': string;
    'icon-wrapper': string;
    'job-info': string;
    'location-icon': string;
    'job-meta-title': string;
    'job-new-tag': string;
    'job-meta-company': string;
    'job-meta-description': string;
    'job-meta-location': string;
    'job-meta-date-relative': string;
    'job-meta-closed-date': string;
};
export default styles;
//# sourceMappingURL=Job.module.scss.d.ts.map