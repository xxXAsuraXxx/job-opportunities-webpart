declare const styles: {
    'job-vacancy-main': string;
    'job-vacancy-bg': string;
    'bg-img': string;
    'job-vacancy-list-wrapper': string;
    'bg--active': string;
};
export default styles;
//# sourceMappingURL=JobVacancyList.module.scss.d.ts.map