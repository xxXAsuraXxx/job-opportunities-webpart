declare const styles: {
    'content-loader': string;
    'loader-icon': string;
    'loader-text': string;
    'loading-spinner': string;
    dots: string;
    spinner__dot: string;
    pulse: string;
    'spinner__dot--one': string;
    'spinner__dot--tw': string;
};
export default styles;
//# sourceMappingURL=Loader.module.scss.d.ts.map