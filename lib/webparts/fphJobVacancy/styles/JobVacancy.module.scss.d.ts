declare const styles: {
    'job-vacancy-load-err-msg': string;
    'job-vacancy-webpart': string;
    'with-box-shadow': string;
    'webpart-nav-btn': string;
    'job-vacancy-heading': string;
};
export default styles;
//# sourceMappingURL=JobVacancy.module.scss.d.ts.map