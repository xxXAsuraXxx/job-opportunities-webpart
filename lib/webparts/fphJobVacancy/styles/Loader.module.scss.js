/* tslint:disable */
require("./Loader.module.css");
var styles = {
    'content-loader': 'content-loader_7af44a37',
    'loader-icon': 'loader-icon_7af44a37',
    'loader-text': 'loader-text_7af44a37',
    'loading-spinner': 'loading-spinner_7af44a37',
    dots: 'dots_7af44a37',
    spinner__dot: 'spinner__dot_7af44a37',
    pulse: 'pulse_7af44a37',
    'spinner__dot--one': 'spinner__dot--one_7af44a37',
    'spinner__dot--tw': 'spinner__dot--tw_7af44a37',
};
export default styles;
/* tslint:enable */ 
//# sourceMappingURL=Loader.module.scss.js.map