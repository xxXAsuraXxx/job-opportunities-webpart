var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import styles from '../../styles/Job.module.scss';
import JobVacancyService from '../../service/JobVacancyService';
import * as moment from 'moment';
var Job = /** @class */ (function (_super) {
    __extends(Job, _super);
    function Job(props) {
        var _this = _super.call(this, props) || this;
        _this.setting = {
            max_description_length: 150,
            max_title_length: 100,
        };
        _this.fetchJobLink = function (siteId, jobId, appName) {
            return _this._JobVacancyService.GetJobLink(siteId, jobId, appName);
        };
        _this.renderJobPostDate = function (jobPublishDateString) {
            return React.createElement("span", null, moment("" + new Date(jobPublishDateString)).fromNow());
        };
        _this.renderDescription = function (description) {
            return truncate(description, _this.setting.max_description_length);
        };
        /**
         * Function that gets the visual display of countries
         */
        _this.getLocationDisplay = function () {
            var matchedLocations = {};
            var categories = _this.props.job.categories;
            var jobCategories = _this.props.jobCategories;
            var jobCategoriesDictionary = jobCategories[jobCategories.length - 1].values;
            // This assumes that the location category is always the last item in the category list
            var locationCategories = categories[categories.length - 1];
            var locationCategoryIds = locationCategories.values;
            var locationName = locationCategoryIds.map(function (locationCategoryIds) {
                var matchedCategory = jobCategoriesDictionary.map(function (location) {
                    // if there are states
                    if (location.values.length) {
                        var subLocation = location.values.map(function (subLoc) {
                            if (subLoc.id === locationCategoryIds) {
                                // if first time meet the country
                                if (!matchedLocations["" + location.name]) {
                                    matchedLocations["" + location.name] = {};
                                    matchedLocations["" + location.name]["states"] = [];
                                    matchedLocations["" + location.name]["states"].push({
                                        'id': subLoc.id,
                                        'name': subLoc.name
                                    });
                                }
                                else {
                                    // add it under existing country
                                    matchedLocations["" + location.name]["states"].push({
                                        'id': subLoc.id,
                                        'name': subLoc.name
                                    });
                                }
                                return;
                            }
                        });
                        // if just countries
                    }
                    else {
                        if (locationCategoryIds === location.id) {
                            if (!matchedLocations["" + location.name]) {
                                matchedLocations["" + location.name] = {};
                            }
                            return;
                        }
                    }
                });
            });
            // console.log(matchedLocations);
            return buildCountryDisplay(matchedLocations);
        };
        _this._JobVacancyService = new JobVacancyService();
        _this.state = {
            jobLink: undefined,
            fetchingLink: true
        };
        return _this;
    }
    Job.prototype.componentDidMount = function () {
        var _this = this;
        var _a = this.props, site = _a.site, job = _a.job;
        var jobId = job.id;
        var appName = site.app;
        var siteId = site.site.id;
        this.fetchJobLink(siteId, jobId, appName).then(function (response) {
            _this.setState({
                jobLink: response.url,
                fetchingLink: false
            });
        });
    };
    Job.prototype.render = function () {
        var _a = this.props, job = _a.job, showDescription = _a.showDescription;
        var _b = this.state, jobLink = _b.jobLink, fetchingLink = _b.fetchingLink;
        return (React.createElement("div", { className: styles["job-entry"], "data-job": JSON.stringify(job) },
            fetchingLink && React.createElement("div", { className: styles["mask"] }, " preparing link"),
            React.createElement("div", { className: styles.wrapper },
                React.createElement("div", { className: styles["job-info"] },
                    React.createElement("a", { className: styles["job-meta-title"], href: jobLink, target: "_blank" }, job.title),
                    job.isNew && React.createElement(NewJobTag, null),
                    React.createElement("div", { className: styles["job-meta-location"] },
                        React.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", height: "90", width: "90", viewBox: "0 0 90 90" },
                            React.createElement("g", null,
                                React.createElement("path", { id: "path1", transform: "rotate(0,45,45) translate(19.5874981880188,7) scale(2.375,2.375)  ", fill: "#3D6187", d: "M10.7,4.5C7.4999914,4.5 4.9000191,7.3000183 4.9000191,10.800018 4.9000191,14.300018 7.4999914,17.100006 10.7,17.100006 13.900009,17.100006 16.499982,14.300018 16.499982,10.800018 16.599957,7.3000183 13.999984,4.5 10.7,4.5z M10.7,0C16.599957,0 21.400001,5.2000122 21.400001,11.5 21.400001,17.899994 10.7,32 10.7,32 10.7,32 5.5881173E-08,17.899994 0,11.5 5.5881173E-08,5.2000122 4.7999825,0 10.7,0z" }))),
                        React.createElement("span", null, this.getLocationDisplay())),
                    showDescription && React.createElement("p", { className: styles["job-meta-description"] }, this.renderDescription(job.description)),
                    React.createElement("div", { className: styles["job-meta-date-relative"] },
                        this.renderJobPostDate(job.availability.jobPublishDates.publishDate),
                        job.availability.applicationCloseDates && React.createElement("span", { className: styles["job-meta-closed-date"] }, " - Close Date: " + moment("" + new Date(job.availability.applicationCloseDates.closeDate)).format('DD MMM YYYY HH:MM')))))));
    };
    return Job;
}(React.Component));
export default Job;
var NewJobTag = function (props) {
    return (React.createElement("span", { className: styles['job-new-tag'] }, "New"));
};
/**
 * Truncate the rawtext with specified size
 * @param rawText
 * @param size
 */
function truncate(rawText, size) {
    if (rawText.length > size) {
        return rawText.substr(0, size) + " ...";
    }
    else {
        return rawText;
    }
}
/**
 * A country/states display builder
 * @param locations
 */
function buildCountryDisplay(locations) {
    var countryStringBuilder = '';
    var max_country_states_display_length = 3;
    for (var location in locations) {
        if (locations.hasOwnProperty(location)) {
            var currentLocation = locations[location];
            if (currentLocation.states) {
                var states = currentLocation.states;
                if (states.length > max_country_states_display_length) {
                    states = states.slice(0, max_country_states_display_length);
                }
                for (var j = 0; j < states.length; j++) {
                    var name_1 = states[j].name;
                    countryStringBuilder += name_1 + ","; // e.g  Irvine, Yellow Stone,
                }
            }
        }
        countryStringBuilder += "" + location; // e.g Irvin, Yellow Stone, US, New Zealand
    }
    console.log(countryStringBuilder);
    return countryStringBuilder;
}
//# sourceMappingURL=Job.js.map