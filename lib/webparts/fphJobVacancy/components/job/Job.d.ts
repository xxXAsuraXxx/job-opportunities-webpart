import * as React from 'react';
import { IJobProps } from '../job/IJobProps';
export interface IJobState {
    jobLink: string;
    fetchingLink: boolean;
}
export default class Job extends React.Component<IJobProps, IJobState> {
    private _JobVacancyService;
    private setting;
    constructor(props: any);
    componentDidMount(): void;
    fetchJobLink: (siteId: any, jobId: any, appName: any) => Promise<any>;
    renderJobPostDate: (jobPublishDateString: any) => JSX.Element;
    renderDescription: (description: any) => any;
    /**
     * Function that gets the visual display of countries
     */
    getLocationDisplay: () => string;
    render(): React.ReactElement<IJobProps>;
}
//# sourceMappingURL=Job.d.ts.map