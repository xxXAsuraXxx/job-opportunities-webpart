import { IJob } from '../../model/IJob';
import { IJobCategory } from '../../model/IJobCategory';
import { ICareerSite } from '../../model/ICareerSite';
export interface IJobProps {
    job: IJob;
    jobCategories: IJobCategory[];
    site: ICareerSite;
    showDescription: boolean;
}
//# sourceMappingURL=IJobProps.d.ts.map