var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import styles from '../../../styles/Loader.module.scss';
import * as cx from 'classnames';
var Loader = /** @class */ (function (_super) {
    __extends(Loader, _super);
    function Loader() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Loader.prototype.render = function () {
        var logo = require("../../../asset/icon/svg/jobVacancyLogo.svg");
        return (React.createElement("div", { className: styles["content-loader"] },
            React.createElement("span", { className: styles["loader-icon"] },
                React.createElement("img", { src: logo })),
            React.createElement("span", { className: styles["loader-text"] }, "Loading Jobs"),
            React.createElement("div", { className: styles["loading-spinner"] },
                React.createElement("div", { className: styles["dots"] },
                    React.createElement("i", { className: cx(styles["spinner__dot"], styles["spinner__dot--one"]) }),
                    React.createElement("i", { className: cx(styles["spinner__dot"], styles["spinner__dot--two"]) }),
                    React.createElement("i", { className: cx(styles["spinner__dot"], styles["spinner__dot--three"]) })))));
    };
    return Loader;
}(React.Component));
export default Loader;
//# sourceMappingURL=Loader.js.map