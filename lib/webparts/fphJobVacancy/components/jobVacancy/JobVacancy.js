var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import styles from '../../styles/JobVacancy.module.scss';
import * as moment from 'moment';
/// import components
import JobVacancyList from '../jobVacancyList/JobVacancyList';
import Loader from '../common/loader/Loader';
//import Service
import JobVacancyService from '../../service/JobVacancyService';
var JobVacancy = /** @class */ (function (_super) {
    __extends(JobVacancy, _super);
    function JobVacancy(props, state) {
        var _this = _super.call(this, props) || this;
        _this.fetchMasterData = function () {
            _this.setState({ isLoading: true });
            Promise.all([_this.fetchOpenJobs(), _this.fetchJobCategories(), _this.fetchCareerSites()]).then(function (values) {
                var jobs = values[0].results;
                var sites = values[2];
                var jobCategories = values[1];
                if (!sites || !jobCategories)
                    throw new Error('Something happened during fetching');
                var top5ActiveJobs = _this.getValidJobListing(jobs);
                var activeCareerSite = _this.parseCareerSites(sites);
                _this.setState({
                    jobList: top5ActiveJobs,
                    jobCategories: jobCategories,
                    careerSite: activeCareerSite,
                    isLoading: false
                });
            }).catch(function (error) {
                _this.setState({ error: error, isLoading: false });
            });
        };
        _this.getValidJobListing = function (rawjobs) {
            var numberOfJobsToShow = _this.props.numberOfJobsToShow;
            var valideTop5Jobs = rawjobs
                .filter(function (job) {
                var availability = job.availability;
                var isJobActive = true;
                if (availability.applicationCloseDates) {
                    var closeDate = new Date(availability.applicationCloseDates.closeDate);
                    var today = new Date();
                    isJobActive = (today <= closeDate);
                }
                return ((availability.availableInternally || availability.availableExternally) && isJobActive);
            })
                .sort(function (a, b) {
                var aDate = new Date(a.availability.jobPublishDates.publishDate);
                var bDate = new Date(b.availability.jobPublishDates.publishDate);
                return bDate > aDate ? 1 : -1;
            })
                .slice(0, numberOfJobsToShow)
                .map(function (job) {
                var oneHour = 60 * 60 * 1000;
                var localDate = new Date(job.availability.jobPublishDates.publishDate);
                var now = new Date();
                if (moment(localDate).isAfter(moment().subtract(10, 'hours'))) {
                    job['isNew'] = true;
                }
                else {
                    if (job['isNew']) {
                        delete job.isNew;
                    }
                }
                return job;
            });
            return valideTop5Jobs;
        };
        _this.parseCareerSites = function (sites) {
            var primarySite = sites[0];
            return {
                app: primarySite.app,
                site: {
                    id: primarySite.careerSites[0].id,
                    label: primarySite.careerSites[0].label,
                }
            };
        };
        _this.fetchJobCategories = function () {
            return _this._JobVacancyService.GetAllJobsCategories();
        };
        _this.fetchOpenJobs = function () {
            return _this._JobVacancyService.GetAllOpenJobs();
        };
        _this.fetchCareerSites = function () {
            return _this._JobVacancyService.GetCareerSiteInfo();
        };
        _this._JobVacancyService = new JobVacancyService();
        _this.state = {
            jobList: undefined,
            jobCategories: undefined,
            jobLinks: undefined,
            careerSite: undefined,
            isLoading: true,
            error: undefined
        };
        return _this;
    }
    JobVacancy.prototype.componentDidMount = function () {
        this.fetchMasterData();
    };
    // When receiving props we check if new props numberOfJobsToShow is same as old numberOfJobsToShow
    JobVacancy.prototype.componentWillReceiveProps = function (nextProps) {
        if (nextProps.numberOfJobsToShow !== this.props.numberOfJobsToShow) {
            this.fetchMasterData();
        }
    };
    JobVacancy.prototype.render = function () {
        var _a = this.state, jobList = _a.jobList, isLoading = _a.isLoading, jobCategories = _a.jobCategories, careerSite = _a.careerSite, error = _a.error;
        var _b = this.props, showAllJobs = _b.showAllJobs, showDescription = _b.showDescription, enableBackground = _b.enableBackground, currentSelectedBgColor = _b.currentSelectedBgColor, selectedBgImage = _b.selectedBgImage;
        if (isLoading) {
            return React.createElement(Loader, null);
        }
        if (error) {
            return React.createElement("span", { className: styles['job-vacancy-load-err-msg'] }, "Job Vacancy is current unavailable. please contact the intranet team for information");
        }
        return (React.createElement("div", { className: styles["job-vacancy-webpart"] },
            React.createElement("h1", { className: styles['job-vacancy-heading'] }, this.props.description),
            showAllJobs && React.createElement("a", { href: "https://jobs.fphcare.com/home", className: styles['webpart-nav-btn'], target: "_blank" }, "See all"),
            React.createElement(JobVacancyList, { jobs: jobList, categories: jobCategories, site: careerSite, showDescription: showDescription, enableBackground: enableBackground, selectBgColor: currentSelectedBgColor, selectedBgImage: selectedBgImage })));
    };
    return JobVacancy;
}(React.Component));
export default JobVacancy;
//# sourceMappingURL=JobVacancy.js.map