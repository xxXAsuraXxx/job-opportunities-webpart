import * as React from 'react';
import { IJobVacancyProps } from './IJobVacancyProps';
import { IJob } from '../../model/IJob';
import { IJobCategory } from '../../model/IJobCategory';
import { ICareerSite } from '../../model/ICareerSite';
export interface IJobVacancyState {
    jobList: IJob[];
    jobCategories: IJobCategory[];
    careerSite: ICareerSite;
    jobLinks: any;
    isLoading: Boolean;
    error: any;
}
export default class JobVacancy extends React.Component<IJobVacancyProps, IJobVacancyState> {
    private _JobVacancyService;
    constructor(props: IJobVacancyProps, state: IJobVacancyState);
    componentDidMount(): void;
    componentWillReceiveProps(nextProps: any): void;
    fetchMasterData: () => void;
    getValidJobListing: (rawjobs: any) => IJob[];
    parseCareerSites: (sites: any) => ICareerSite;
    fetchJobCategories: () => Promise<any>;
    fetchOpenJobs: () => Promise<any>;
    fetchCareerSites: () => Promise<any>;
    render(): React.ReactElement<IJobVacancyProps>;
}
//# sourceMappingURL=JobVacancy.d.ts.map