export interface IJobVacancyProps {
    description: string;
    numberOfJobsToShow: number;
    showAllJobs: boolean;
    showDescription: boolean;
    enableBackground: boolean;
    currentSelectedBgColor: string;
    selectedBgImage: string;
}
//# sourceMappingURL=IJobVacancyProps.d.ts.map