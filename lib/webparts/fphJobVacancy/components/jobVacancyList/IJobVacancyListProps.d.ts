import { IJob } from '../../model/IJob';
import { IJobCategory } from '../../model/IJobCategory';
import { ICareerSite } from '../../model/ICareerSite';
export interface IJobVacancyListProps {
    jobs: IJob[];
    categories: IJobCategory[];
    site: ICareerSite;
    showDescription: boolean;
    enableBackground: boolean;
    selectBgColor: string;
    selectedBgImage: string;
}
//# sourceMappingURL=IJobVacancyListProps.d.ts.map