import * as React from 'react';
import { IJobVacancyListProps } from './IJobVacancyListProps';
export interface IJobVacancyListState {
}
export default class JobVacancyList extends React.Component<IJobVacancyListProps, IJobVacancyListState> {
    constructor(props: IJobVacancyListProps);
    componentDidMount(): void;
    renderJobList: () => JSX.Element;
    render(): React.ReactElement<IJobVacancyListProps>;
}
//# sourceMappingURL=JobVacancyList.d.ts.map