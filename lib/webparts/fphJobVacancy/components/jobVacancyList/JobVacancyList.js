var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import styles from '../../styles/JobVacancyList.module.scss';
import * as cx from 'classnames';
import Job from '../job/Job';
var JobVacancyList = /** @class */ (function (_super) {
    __extends(JobVacancyList, _super);
    function JobVacancyList(props) {
        var _this = _super.call(this, props) || this;
        _this.renderJobList = function () {
            var _a;
            var _b = _this.props, jobs = _b.jobs, categories = _b.categories, site = _b.site, showDescription = _b.showDescription, enableBackground = _b.enableBackground;
            var listWrapperStyle = cx((_a = {},
                _a[styles['job-vacancy-list-wrapper']] = true,
                _a[styles['bg--active']] = enableBackground,
                _a));
            return (React.createElement("div", { className: listWrapperStyle },
                React.createElement("div", { className: styles['job-vacancy-list-container'] }, jobs.map(function (singleJobEntry, i) {
                    return (React.createElement(Job, { key: i, job: singleJobEntry, jobCategories: categories, site: site, showDescription: showDescription }));
                }))));
        };
        _this.state = {};
        return _this;
    }
    JobVacancyList.prototype.componentDidMount = function () {
    };
    JobVacancyList.prototype.render = function () {
        var _a = this.props, enableBackground = _a.enableBackground, selectBgColor = _a.selectBgColor, selectedBgImage = _a.selectedBgImage;
        var style = {
            backgroundColor: selectBgColor,
            backgroundImage: "url(" + selectedBgImage + ")",
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
        };
        return (React.createElement("div", { className: styles['job-vacancy-main'] },
            enableBackground && React.createElement("span", { className: styles['job-vacancy-bg'], style: style }),
            this.renderJobList()));
    };
    return JobVacancyList;
}(React.Component));
export default JobVacancyList;
//# sourceMappingURL=JobVacancyList.js.map