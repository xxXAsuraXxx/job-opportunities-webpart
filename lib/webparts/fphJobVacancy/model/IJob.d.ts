export interface IJob {
    id: number;
    title: string;
    code: string;
    shortSummary: string;
    description: string;
    company: string;
    availability: IAvailability;
    country: string;
    categories: ICategory[];
    isNew: boolean;
}
export interface IAvailability {
    availableInternally: boolean;
    availableExternally: boolean;
    jobPublishDates: IJobPublishDates;
    applicationCloseDates: IJobCloseDates;
}
export interface IJobPublishDates {
    publishDate: string;
}
export interface IJobCloseDates {
    closeDate: string;
}
export interface ICategory {
    category: number;
    values: number[];
}
//# sourceMappingURL=IJob.d.ts.map