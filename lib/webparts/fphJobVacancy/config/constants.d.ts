export declare const config: {
    END_POINT_HOST: string;
    GET_ALL_JOB_OPEN: string;
    GET_ALL_JOB_CATEGORIES: string;
    GET_CAREER_SITES: string;
    GET_JOB_LINK: string;
};
//# sourceMappingURL=constants.d.ts.map