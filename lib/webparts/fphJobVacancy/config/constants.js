//Configuration for Job vacancy
export var config = {
    END_POINT_HOST: 'https://api-intranet-uat.fphcare.com/api/',
    // end points for job vacancies webpart
    GET_ALL_JOB_OPEN: "JobVacancy/GetAllOpenJobListings",
    GET_ALL_JOB_CATEGORIES: "JobVacancy/GetAllJobCategories",
    GET_CAREER_SITES: "JobVacancy/GetAllCareerSites",
    GET_JOB_LINK: 'JobVacancy/GetJobLink'
};
//# sourceMappingURL=constants.js.map