import IJobVacancyService from './interface/IJobVacancyService';
export default class JobVacancyService implements IJobVacancyService {
    /**
     * Gets all open jobs
     * @returns all open jobs
     */
    GetAllOpenJobs(): Promise<any>;
    /**
     * Gets all jobs categories
     * @returns all jobs categories
     */
    GetAllJobsCategories(): Promise<any>;
    /**
     * Gets career site info
     * @returns career site info
     */
    GetCareerSiteInfo(): Promise<any>;
    /**
     * Gets job link
     * @param siteId
     * @param jobId
     * @param appName
     * @returns job link
     */
    GetJobLink(siteId: any, jobId: any, appName: any): Promise<any>;
}
//# sourceMappingURL=JobVacancyService.d.ts.map