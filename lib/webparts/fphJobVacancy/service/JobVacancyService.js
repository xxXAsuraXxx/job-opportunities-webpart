import { config } from '../config/constants';
//import Util
import { parseJson } from '../util/request';
var JobVacancyService = /** @class */ (function () {
    function JobVacancyService() {
    }
    /**
     * Gets all open jobs
     * @returns all open jobs
     */
    JobVacancyService.prototype.GetAllOpenJobs = function () {
        return fetch("" + config.END_POINT_HOST + config.GET_ALL_JOB_OPEN).then(parseJson);
    };
    /**
     * Gets all jobs categories
     * @returns all jobs categories
     */
    JobVacancyService.prototype.GetAllJobsCategories = function () {
        return fetch("" + config.END_POINT_HOST + config.GET_ALL_JOB_CATEGORIES).then(parseJson);
    };
    /**
     * Gets career site info
     * @returns career site info
     */
    JobVacancyService.prototype.GetCareerSiteInfo = function () {
        return fetch("" + config.END_POINT_HOST + config.GET_CAREER_SITES).then(parseJson);
    };
    /**
     * Gets job link
     * @param siteId
     * @param jobId
     * @param appName
     * @returns job link
     */
    JobVacancyService.prototype.GetJobLink = function (siteId, jobId, appName) {
        return fetch("" + config.END_POINT_HOST + config.GET_JOB_LINK + "?siteId=" + siteId + "&jobId=" + jobId + "&appName=" + appName).then(parseJson);
    };
    return JobVacancyService;
}());
export default JobVacancyService;
//# sourceMappingURL=JobVacancyService.js.map