var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { PropertyPaneTextField, PropertyPaneDropdown, PropertyPaneCheckbox } from '@microsoft/sp-property-pane';
import { PropertyFieldPicturePicker } from 'sp-client-custom-fields/lib/PropertyFieldPicturePicker';
import { PropertyFieldColorPicker, PropertyFieldColorPickerStyle } from '@pnp/spfx-property-controls/lib/PropertyFieldColorPicker';
import * as strings from 'FphJobVacancyWebPartStrings';
import JobVacancy from './components/jobVacancy/JobVacancy';
var FphJobVacancyWebPart = /** @class */ (function (_super) {
    __extends(FphJobVacancyWebPart, _super);
    function FphJobVacancyWebPart() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FphJobVacancyWebPart.prototype.render = function () {
        var element = React.createElement(JobVacancy, {
            description: this.properties.description,
            numberOfJobsToShow: this.properties.numberOfJobsToShow,
            showAllJobs: this.properties.showAllJobs,
            showDescription: this.properties.showDescription,
            enableBackground: this.properties.enableBackground,
            currentSelectedBgColor: this.properties.currentBgColor,
            selectedBgImage: this.properties.bgImage
        });
        ReactDom.render(element, this.domElement);
    };
    FphJobVacancyWebPart.prototype.onDispose = function () {
        ReactDom.unmountComponentAtNode(this.domElement);
    };
    Object.defineProperty(FphJobVacancyWebPart.prototype, "dataVersion", {
        get: function () {
            return Version.parse('1.0');
        },
        enumerable: true,
        configurable: true
    });
    FphJobVacancyWebPart.prototype.getPropertyPaneConfiguration = function () {
        return {
            pages: [
                {
                    header: {
                        description: strings.PropertyPaneDescription
                    },
                    displayGroupsAsAccordion: true,
                    groups: [
                        {
                            groupName: "Description",
                            groupFields: [
                                PropertyPaneTextField('description', {
                                    label: strings.DescriptionFieldLabel
                                }),
                            ]
                        },
                        {
                            groupName: "Job vacancy configuration",
                            groupFields: [
                                PropertyPaneDropdown('numberOfJobsToShow', {
                                    label: "Job vacancy display limit",
                                    options: [
                                        { key: 1, text: '1' },
                                        { key: 2, text: '2' },
                                        { key: 3, text: '3' },
                                        { key: 4, text: '4' }
                                    ],
                                    selectedKey: 3,
                                }),
                                PropertyPaneCheckbox('showAllJobs', {
                                    checked: true,
                                    text: "Show all jobs link"
                                }),
                                PropertyPaneCheckbox('showDescription', {
                                    checked: false,
                                    text: "Show Job Description"
                                }),
                                PropertyPaneCheckbox('enableBackground', {
                                    checked: true,
                                    text: "Enable webpart background"
                                }),
                                PropertyFieldColorPicker('currentBgColor', {
                                    label: 'Background Color',
                                    selectedColor: this.properties.currentBgColor,
                                    onPropertyChange: this.onPropertyPaneFieldChanged,
                                    properties: this.properties,
                                    disabled: false,
                                    isHidden: !this.properties.enableBackground,
                                    alphaSliderHidden: false,
                                    style: PropertyFieldColorPickerStyle.Full,
                                    iconName: 'Precipitation',
                                    key: 'colorFieldId'
                                }),
                                PropertyFieldPicturePicker('bgImage', {
                                    label: 'Select a background Image',
                                    initialValue: this.properties.bgImage,
                                    readOnly: true,
                                    previewImage: true,
                                    allowedFileExtensions: '.gif,.jpg,.jpeg,.bmp,.dib,.tif,.tiff,.ico,.png',
                                    disabled: !this.properties.enableBackground,
                                    onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                                    render: this.render.bind(this),
                                    disableReactivePropertyChanges: this.disableReactivePropertyChanges,
                                    properties: this.properties,
                                    context: this.context,
                                    onGetErrorMessage: null,
                                    deferredValidationTime: 0,
                                    key: 'picturePickerFieldId'
                                })
                            ]
                        }
                    ]
                }
            ]
        };
    };
    return FphJobVacancyWebPart;
}(BaseClientSideWebPart));
export default FphJobVacancyWebPart;
//# sourceMappingURL=FphJobVacancyWebPart.js.map