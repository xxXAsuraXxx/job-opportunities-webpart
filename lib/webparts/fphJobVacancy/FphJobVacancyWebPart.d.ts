import { Version } from '@microsoft/sp-core-library';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { IPropertyPaneConfiguration } from '@microsoft/sp-property-pane';
export interface IFphJobVacancyWebPartProps {
    description: string;
    numberOfJobsToShow: number;
    showAllJobs: boolean;
    showDescription: boolean;
    enableBackground: boolean;
    currentBgColor: string;
    bgImage: string;
}
export default class FphJobVacancyWebPart extends BaseClientSideWebPart<IFphJobVacancyWebPartProps> {
    render(): void;
    protected onDispose(): void;
    protected readonly dataVersion: Version;
    protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration;
}
//# sourceMappingURL=FphJobVacancyWebPart.d.ts.map