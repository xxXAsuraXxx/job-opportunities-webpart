import * as React from 'react';
import styles from '../../styles/JobVacancyList.module.scss';
import * as cx from 'classnames';
import { IJobVacancyListProps } from './IJobVacancyListProps';
import Job from '../job/Job';

export interface IJobVacancyListState {

}

export default class JobVacancyList extends React.Component<IJobVacancyListProps, IJobVacancyListState> {

    constructor(props: IJobVacancyListProps) {
        super(props);

        this.state = {

        }
    }

    componentDidMount() {
    }

    renderJobList = () => {
        const { jobs, categories, site, showDescription, enableBackground } = this.props;
        const listWrapperStyle = cx({
            [styles['job-vacancy-list-wrapper']]: true,
            [styles['bg--active']]: enableBackground
        })

        return (
            <div className={listWrapperStyle}>
                <div className={styles['job-vacancy-list-container']}>
                    {
                        jobs.map((singleJobEntry, i) => {
                            return (
                                <Job key={i} job={singleJobEntry} jobCategories={categories} site={site} showDescription={showDescription} />
                            );
                        })
                    }
                </div>
            </div>
        );
    }

    public render(): React.ReactElement<IJobVacancyListProps> {
        const { enableBackground, selectBgColor, selectedBgImage } = this.props;
        const style = {
            backgroundColor: selectBgColor,
            backgroundImage: `url(${selectedBgImage})`,
            backgroundPosition:'center',
            backgroundSize: 'cover',
            backgroundRepeat:'no-repeat'
        }
        return (
            <div className={styles['job-vacancy-main']}>
                {enableBackground && <span className={styles['job-vacancy-bg']} style={style}></span>}
                {this.renderJobList()}
            </div>
        );
    }

}