import * as React from 'react';
import styles from '../../styles/JobVacancy.module.scss';
import { escape } from '@microsoft/sp-lodash-subset';

import { IJobVacancyProps } from './IJobVacancyProps';

import * as moment from 'moment';
import * as cx from 'classnames';
/// import models
import { IJob } from '../../model/IJob';
import { IJobCategory } from '../../model/IJobCategory';
import { ICareerSite } from '../../model/ICareerSite';
/// import components
import JobVacancyList from '../jobVacancyList/JobVacancyList';
import Loader from '../common/loader/Loader';

//import Service
import JobVacancyService from '../../service/JobVacancyService';
import IJobVacancyService from '../../service/JobVacancyService';

export interface IJobVacancyState {
  jobList: IJob[];
  jobCategories: IJobCategory[];
  careerSite: ICareerSite;
  jobLinks: any;
  isLoading: Boolean;
  error: any;
}

export default class JobVacancy extends React.Component<IJobVacancyProps, IJobVacancyState> {

  private _JobVacancyService: IJobVacancyService;

  constructor(props: IJobVacancyProps, state: IJobVacancyState) {
    super(props);
    this._JobVacancyService = new JobVacancyService();
    this.state = {
      jobList: undefined,
      jobCategories: undefined,
      jobLinks: undefined,
      careerSite: undefined,
      isLoading: true,
      error: undefined
    }

  }
  componentDidMount() {
    this.fetchMasterData();
  }

  // When receiving props we check if new props numberOfJobsToShow is same as old numberOfJobsToShow
  componentWillReceiveProps(nextProps) {
    if (nextProps.numberOfJobsToShow !== this.props.numberOfJobsToShow) {
      this.fetchMasterData();
    }
  }


  fetchMasterData = () => {
    this.setState({ isLoading: true });
    Promise.all([this.fetchOpenJobs(), this.fetchJobCategories(), this.fetchCareerSites()]).then(values => {

      var jobs = values[0].results;
      var sites = values[2];
      var jobCategories = values[1];
      if(!sites || !jobCategories) throw new Error ('Something happened during fetching');
      var top5ActiveJobs = this.getValidJobListing(jobs);
      var activeCareerSite = this.parseCareerSites(sites);

      this.setState({
        jobList: top5ActiveJobs,
        jobCategories: jobCategories,
        careerSite: activeCareerSite,
        isLoading: false
      });

    }).catch((error) => {
      this.setState({ error: error, isLoading: false })
    })
  }

  getValidJobListing = (rawjobs): IJob[] => {
    const { numberOfJobsToShow } = this.props;
    var valideTop5Jobs: IJob[] = rawjobs
      .filter((job) => {
        const { availability } = job;
        let isJobActive: boolean = true;
        if (availability.applicationCloseDates) {
          let closeDate = new Date(availability.applicationCloseDates.closeDate);
          let today = new Date();
          isJobActive = (today <= closeDate);
        }
        return ((availability.availableInternally || availability.availableExternally) && isJobActive)
      })
      .sort((a, b) => {
        var aDate = new Date(a.availability.jobPublishDates.publishDate);
        var bDate = new Date(b.availability.jobPublishDates.publishDate);
        return bDate > aDate ? 1 : -1;
      })
      .slice(0, numberOfJobsToShow)
      .map(job => {
        var oneHour: number = 60 * 60 * 1000;
        var localDate: Date = new Date(job.availability.jobPublishDates.publishDate);
        var now: Date = new Date();
        if (moment(localDate).isAfter(moment().subtract(10, 'hours'))) {
          job['isNew'] = true;
        } else {
          if (job['isNew']) {
            delete job.isNew;
          }
        }
        return job;
      });

    return valideTop5Jobs;
  }


  parseCareerSites = (sites): ICareerSite => {
    var primarySite = sites[0];
    return {
      app: primarySite.app,
      site: {
        id: primarySite.careerSites[0].id,
        label: primarySite.careerSites[0].label,
      }
    } as ICareerSite
  }

  fetchJobCategories = (): Promise<any> => {
    return this._JobVacancyService.GetAllJobsCategories();
  }

  fetchOpenJobs = (): Promise<any> => {
    return this._JobVacancyService.GetAllOpenJobs();
  }

  fetchCareerSites = (): Promise<any> => {
    return this._JobVacancyService.GetCareerSiteInfo();
  }


  public render(): React.ReactElement<IJobVacancyProps> {

    const { jobList, isLoading, jobCategories, careerSite, error } = this.state;
    const { showAllJobs, showDescription, enableBackground, currentSelectedBgColor, selectedBgImage } = this.props;

    if (isLoading) {
      return <Loader />;
    }
    if (error) {
      return <span className={styles['job-vacancy-load-err-msg']}>Job Vacancy is current unavailable. please contact the intranet team for information</span>
    }

    return (
      <div className={styles["job-vacancy-webpart"]}>
        <h1 className={styles['job-vacancy-heading']}>{this.props.description}</h1>
        {showAllJobs && <a href="https://jobs.fphcare.com/home" className={styles['webpart-nav-btn']} target="_blank">See all</a>}

        <JobVacancyList
          jobs={jobList}
          categories={jobCategories}
          site={careerSite}
          showDescription={showDescription}
          enableBackground={enableBackground}
          selectBgColor={currentSelectedBgColor}
          selectedBgImage={selectedBgImage}
        />


      </div>

    );
  }
}


