import { ColorPicker } from 'office-ui-fabric-react/lib/index';

export interface IJobVacancyProps {
  description: string;
  numberOfJobsToShow: number;
  showAllJobs: boolean;
  showDescription:boolean;
  enableBackground:boolean;
  currentSelectedBgColor:string;
  selectedBgImage:string;
}
