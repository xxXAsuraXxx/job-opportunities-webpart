import * as React from 'react';
import styles from '../../../styles/Loader.module.scss';
import * as cx from 'classnames';

export default class Loader extends React.Component<{}, {}> {

  public render(): React.ReactElement<{}> {
    const logo = require("../../../asset/icon/svg/jobVacancyLogo.svg") as string;
    
    return (
      <div className={styles["content-loader"]}>
        <span className={styles["loader-icon"]}>
          <img src={logo} />
        </span>
        <span className={styles["loader-text"]}>Loading Jobs</span>
        <div className={styles["loading-spinner"]}>
          <div className={styles["dots"]}>
            <i className={cx(styles["spinner__dot"], styles["spinner__dot--one"])}></i>
            <i className={cx(styles["spinner__dot"], styles["spinner__dot--two"])}></i>
            <i className={cx(styles["spinner__dot"], styles["spinner__dot--three"])}></i>
          </div>
        </div>
      </div>
    );
  }

}