
import * as React from 'react';
import styles from '../../styles/Job.module.scss';
import { IJobProps } from '../job/IJobProps';
import JobVacancyService from '../../service/JobVacancyService';
import IJobVacancyService from '../../service/interface/IJobVacancyService';
import * as moment from 'moment';

export interface IJobState {
    jobLink: string;
    fetchingLink: boolean;
}

export default class Job extends React.Component<IJobProps, IJobState>{
    // Dependcy inject job vacancy service
    private _JobVacancyService: IJobVacancyService;

    private setting = {
        max_description_length: 150,
        max_title_length: 100,
    }

    constructor(props) {
        super(props);
        this._JobVacancyService = new JobVacancyService();
        this.state = {
            jobLink: undefined,
            fetchingLink: true
        }
    }

    componentDidMount() {
        const { site, job } = this.props;
        const jobId = job.id;
        const appName = site.app;
        const siteId = site.site.id;
        this.fetchJobLink(siteId, jobId, appName).then(response => {
            this.setState({
                jobLink: response.url,
                fetchingLink: false
            })
        })
    }

    fetchJobLink = (siteId, jobId, appName) => {
        return this._JobVacancyService.GetJobLink(siteId, jobId, appName);
    }

    renderJobPostDate = (jobPublishDateString) => {
        return <span>{moment(`${new Date(jobPublishDateString)}`).fromNow()}</span>
    }

    renderDescription = (description) => {
        return truncate(description, this.setting.max_description_length)
    }

    /**
     * Function that gets the visual display of countries
     */
    getLocationDisplay = () => {

        let matchedLocations = {};
        const { categories } = this.props.job;
        const { jobCategories } = this.props;
        var jobCategoriesDictionary = jobCategories[jobCategories.length - 1].values;
        // This assumes that the location category is always the last item in the category list
        var locationCategories = categories[categories.length - 1];
        var locationCategoryIds = locationCategories.values;
        var locationName = locationCategoryIds.map(locationCategoryIds => {
            var matchedCategory = jobCategoriesDictionary.map((location) => {

                // if there are states
                if (location.values.length) {
                    var subLocation = location.values.map((subLoc) => {
                        if (subLoc.id === locationCategoryIds) {
                            // if first time meet the country
                            if (!matchedLocations[`${location.name}`]) {
                                matchedLocations[`${location.name}`] = {};
                                matchedLocations[`${location.name}`]["states"] = [];
                                matchedLocations[`${location.name}`]["states"].push({
                                    'id': subLoc.id,
                                    'name': subLoc.name
                                })
                            } else {
                                // add it under existing country
                                matchedLocations[`${location.name}`]["states"].push({
                                    'id': subLoc.id,
                                    'name': subLoc.name
                                })
                            }

                            return;
                        }
                    })
                    // if just countries
                } else {
                    if (locationCategoryIds === location.id) {
                        if (!matchedLocations[`${location.name}`]) {
                            matchedLocations[`${location.name}`] = {};
                        }
                        return;
                    }

                }

            });

        });
        // console.log(matchedLocations);
        return buildCountryDisplay(matchedLocations);
    }


    public render(): React.ReactElement<IJobProps> {
        const { job, showDescription } = this.props;
        const { jobLink, fetchingLink } = this.state;
        return (
            <div className={styles["job-entry"]} data-job={JSON.stringify(job)}>
                {fetchingLink && <div className={styles["mask"]}> preparing link</div>}
                <div className={styles.wrapper}>
                    <div className={styles["job-info"]}>
                        <a className={styles["job-meta-title"]} href={jobLink} target="_blank">{job.title}</a>
                        {job.isNew && <NewJobTag />}
                        <div className={styles["job-meta-location"]}>
                            <svg xmlns="http://www.w3.org/2000/svg" height="90" width="90" viewBox="0 0 90 90">
                                <g>
                                    <path id="path1" transform="rotate(0,45,45) translate(19.5874981880188,7) scale(2.375,2.375)  " fill="#3D6187" d="M10.7,4.5C7.4999914,4.5 4.9000191,7.3000183 4.9000191,10.800018 4.9000191,14.300018 7.4999914,17.100006 10.7,17.100006 13.900009,17.100006 16.499982,14.300018 16.499982,10.800018 16.599957,7.3000183 13.999984,4.5 10.7,4.5z M10.7,0C16.599957,0 21.400001,5.2000122 21.400001,11.5 21.400001,17.899994 10.7,32 10.7,32 10.7,32 5.5881173E-08,17.899994 0,11.5 5.5881173E-08,5.2000122 4.7999825,0 10.7,0z" />
                                </g>
                            </svg><span>{this.getLocationDisplay()}</span></div>
                        {showDescription && <p className={styles["job-meta-description"]}>{this.renderDescription(job.description)}</p>}

                        <div className={styles["job-meta-date-relative"]}>{this.renderJobPostDate(job.availability.jobPublishDates.publishDate)}{job.availability.applicationCloseDates && <span className={styles["job-meta-closed-date"]}>{` - Close Date: ${moment(`${new Date(job.availability.applicationCloseDates.closeDate)}`).format('DD MMM YYYY HH:MM')}`}</span>}
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

const NewJobTag = (props) => {
    return (
        <span className={styles['job-new-tag']}>New</span>
    );
}
/**
 * Truncate the rawtext with specified size
 * @param rawText 
 * @param size 
 */
function truncate(rawText, size) {

    if (rawText.length > size) {
        return `${rawText.substr(0, size)} ...`;
    } else {
        return rawText;
    }
}

/**
 * A country/states display builder
 * @param locations 
 */
function buildCountryDisplay(locations) {

    var countryStringBuilder = '';
    const max_country_states_display_length = 3;
    for (var location in locations) {
        if (locations.hasOwnProperty(location)) {
            var currentLocation = locations[location];
            if (currentLocation.states) {
                var states = currentLocation.states;
                if (states.length > max_country_states_display_length) {
                    states = states.slice(0, max_country_states_display_length);
                }

                for (var j = 0; j < states.length; j++) {
                    const { name } = states[j];
                    countryStringBuilder += `${name},`; // e.g  Irvine, Yellow Stone,
                }
            }
        }
        countryStringBuilder += `${location}`; // e.g Irvin, Yellow Stone, US, New Zealand
    }
    console.log(countryStringBuilder);
    return countryStringBuilder;
}


