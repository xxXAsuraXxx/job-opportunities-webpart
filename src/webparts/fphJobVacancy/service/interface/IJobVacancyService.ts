

export default interface IJobVacancyService {

    GetAllOpenJobs():Promise<any>,
    GetAllJobsCategories(): Promise<any>,
    GetCareerSiteInfo(): Promise<any>,
    GetJobLink(siteId:string, jobId:string, appName:string): Promise<any>
}