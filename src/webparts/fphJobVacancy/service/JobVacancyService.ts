
import { config } from '../config/constants';
//import Util
import { parseJson } from '../util/request';
import IJobVacancyService from './interface/IJobVacancyService';

export default class JobVacancyService implements IJobVacancyService {

   
    /**
     * Gets all open jobs
     * @returns all open jobs 
     */
    public GetAllOpenJobs(): Promise<any> {
        return fetch(`${config.END_POINT_HOST}${config.GET_ALL_JOB_OPEN}`).then(parseJson);
    }

    /**
     * Gets all jobs categories
     * @returns all jobs categories 
     */
    public GetAllJobsCategories(): Promise<any> {
        return fetch(`${config.END_POINT_HOST}${config.GET_ALL_JOB_CATEGORIES}`).then(parseJson);
    }

    /**
     * Gets career site info
     * @returns career site info 
     */
    public GetCareerSiteInfo(): Promise<any> {
        return fetch(`${config.END_POINT_HOST}${config.GET_CAREER_SITES}`).then(parseJson);
    }

   
    /**
     * Gets job link
     * @param siteId 
     * @param jobId 
     * @param appName 
     * @returns job link 
     */
    public GetJobLink(siteId, jobId, appName): Promise<any> {
        return fetch(`${config.END_POINT_HOST}${config.GET_JOB_LINK}?siteId=${siteId}&jobId=${jobId}&appName=${appName}`).then(parseJson);
    }

}


