import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart
} from '@microsoft/sp-webpart-base';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneDropdown,
  IPropertyPaneDropdownOption,
  IPropertyPaneCheckboxProps,
  PropertyPaneCheckbox

} from '@microsoft/sp-property-pane';
import { PropertyFieldPicturePicker } from 'sp-client-custom-fields/lib/PropertyFieldPicturePicker';
import { PropertyFieldColorPicker, PropertyFieldColorPickerStyle } from '@pnp/spfx-property-controls/lib/PropertyFieldColorPicker';
import * as strings from 'FphJobVacancyWebPartStrings';
import JobVacancy from './components/jobVacancy/JobVacancy';
import { IJobVacancyProps } from './components/jobVacancy/IJobVacancyProps';

export interface IFphJobVacancyWebPartProps {
  description: string;
  numberOfJobsToShow: number;
  showAllJobs: boolean;
  showDescription: boolean;
  enableBackground: boolean;
  currentBgColor: string;
  bgImage: string;
}

export default class FphJobVacancyWebPart extends BaseClientSideWebPart<IFphJobVacancyWebPartProps> {

  public render(): void {
    const element: React.ReactElement<IJobVacancyProps> = React.createElement(
      JobVacancy,
      {
        description: this.properties.description,
        numberOfJobsToShow: this.properties.numberOfJobsToShow,
        showAllJobs: this.properties.showAllJobs,
        showDescription: this.properties.showDescription,
        enableBackground: this.properties.enableBackground,
        currentSelectedBgColor: this.properties.currentBgColor,
        selectedBgImage: this.properties.bgImage
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          displayGroupsAsAccordion:true,
          groups: [
            {
              groupName: "Description",
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),

              ]
            },
            {
              groupName: "Job vacancy configuration",
              groupFields: [
                PropertyPaneDropdown('numberOfJobsToShow', {
                  label: "Job vacancy display limit",
                  options: [
                    { key: 1, text: '1' },
                    { key: 2, text: '2' },
                    { key: 3, text: '3' },
                    { key: 4, text: '4' }
                  ],
                  selectedKey: 3,
                }),
                PropertyPaneCheckbox('showAllJobs', {
                  checked: true,
                  text: "Show all jobs link"
                }),
                PropertyPaneCheckbox('showDescription', {
                  checked: false,
                  text: "Show Job Description"
                }),
                PropertyPaneCheckbox('enableBackground', {
                  checked: true,
                  text: "Enable webpart background"
                }),
                PropertyFieldColorPicker('currentBgColor', {
                  label: 'Background Color',
                  selectedColor: this.properties.currentBgColor,
                  onPropertyChange: this.onPropertyPaneFieldChanged,
                  properties: this.properties,
                  disabled: false,
                  isHidden: !this.properties.enableBackground,
                  alphaSliderHidden: false,
                  style: PropertyFieldColorPickerStyle.Full,
                  iconName: 'Precipitation',
                  key: 'colorFieldId'
                }),
                PropertyFieldPicturePicker('bgImage', {
                  label: 'Select a background Image',
                  initialValue: this.properties.bgImage,
                  readOnly: true,
                  previewImage: true,
                  allowedFileExtensions: '.gif,.jpg,.jpeg,.bmp,.dib,.tif,.tiff,.ico,.png',
                  disabled: !this.properties.enableBackground,
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  render: this.render.bind(this),
                  disableReactivePropertyChanges: this.disableReactivePropertyChanges,
                  properties: this.properties,
                  context: this.context,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'picturePickerFieldId'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
