/* tslint:disable */
require("./JobVacancyList.module.css");
const styles = {
  'job-vacancy-main': 'job-vacancy-main_63d70b19',
  'job-vacancy-bg': 'job-vacancy-bg_63d70b19',
  'bg-img': 'bg-img_63d70b19',
  'job-vacancy-list-wrapper': 'job-vacancy-list-wrapper_63d70b19',
  'bg--active': 'bg--active_63d70b19',
};

export default styles;
/* tslint:enable */