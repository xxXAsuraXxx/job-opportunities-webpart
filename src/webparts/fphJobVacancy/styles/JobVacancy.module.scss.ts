/* tslint:disable */
require("./JobVacancy.module.css");
const styles = {
  'job-vacancy-load-err-msg': 'job-vacancy-load-err-msg_cc3ff677',
  'job-vacancy-webpart': 'job-vacancy-webpart_cc3ff677',
  'with-box-shadow': 'with-box-shadow_cc3ff677',
  'webpart-nav-btn': 'webpart-nav-btn_cc3ff677',
  'job-vacancy-heading': 'job-vacancy-heading_cc3ff677',
};

export default styles;
/* tslint:enable */