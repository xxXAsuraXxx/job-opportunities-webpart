/* tslint:disable */
require("./Job.module.css");
const styles = {
  'job-entry': 'job-entry_790a6d2b',
  '-webkit-mask': 'mask_790a6d2b',
  mask: 'mask_790a6d2b',
  wrapper: 'wrapper_790a6d2b',
  'company-icon': 'company-icon_790a6d2b',
  'icon-wrapper': 'icon-wrapper_790a6d2b',
  'job-info': 'job-info_790a6d2b',
  'location-icon': 'location-icon_790a6d2b',
  'job-meta-title': 'job-meta-title_790a6d2b',
  'job-new-tag': 'job-new-tag_790a6d2b',
  'job-meta-company': 'job-meta-company_790a6d2b',
  'job-meta-description': 'job-meta-description_790a6d2b',
  'job-meta-location': 'job-meta-location_790a6d2b',
  'job-meta-date-relative': 'job-meta-date-relative_790a6d2b',
  'job-meta-closed-date': 'job-meta-closed-date_790a6d2b',
};

export default styles;
/* tslint:enable */