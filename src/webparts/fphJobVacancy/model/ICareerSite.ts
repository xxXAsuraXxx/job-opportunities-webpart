export interface ICareerSite{
    app:string;
    site: ISite;
}

export interface ISite{
    id: number;
    label: string;
}