export interface IJobCategory {

    id: number;
    key: string;
    name: string;
    values: IJobCategoryValue[];

}


export interface IJobCategoryValue {

    id: number;
    available: boolean;
    name: string;
    values: any[];
}