declare interface IFphJobVacancyWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'FphJobVacancyWebPartStrings' {
  const strings: IFphJobVacancyWebPartStrings;
  export = strings;
}
